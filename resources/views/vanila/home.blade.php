@extends('vanila.base')

@section('content')

    @isset($products)
        @foreach($products as $product)

        <div class="col-md-2" style="padding: 10px;">

            <div class="card" style="">
                <a href="{{URL::to('product/'.$product->slug)}}">
                    @if($product->url && is_file(public_path('uploads/products/128X128/'.$product->url))) 
                        <img src="{{asset('public/uploads/products/128X128/'.$product->url)}}" class="card-img-top" alt="...">
                    @else
                        <img src="{{asset('public/assets/img/add_image.png')}}" class="card-img-top" alt="...">
                    @endif
                </a>
                <div class="card-body" style="padding: 6px;">
                    <a href="{{URL::to('product/'.$product->slug)}}"><h5 class="card-title" style="font-size: 12px;">
                            {{substr($product->product_name, 0, 40) . '...'}}</h5>
                        <p class="card-text" style="font-size: 10px;"> {{substr($product->tagline, 0, 20) . '...'}}</p>
                    </a>
                    <button type="button" id="add-to-cart-{{$product->id}}" class="btn btn-primary add-to-cart" style="padding: 0px 3px; font-size: 10px;" data-id="{{$product->id}}">Add to cart</button>
                    <span id="add-to-cart-message-{{$product->id}}" style="font-size: 10px;"></span>
                </div>
            </div>

        </div>

        @endforeach
    @endisset


    <div class="col-md-2" style="padding: 10px;">
        {{ $products->links() }}
    </div>


@endsection