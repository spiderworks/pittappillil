@extends('vanila.base')

@section('content')

<div class="col-md-12" style="padding-top: 10px;">

    <table class="table">
        <thead class="thead-light">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Product</th>
            <th scope="col">Qty</th>
            <th scope="col">Cost</th>
            <th scope="col">Total</th>
            <th scope="col">Remove</th>
        </tr>
        </thead>
        <tbody>

        @isset($cart) @php $i=0;$total=0;@endphp
        @foreach($cart as $obj)

            <tr id="cart-row-{{$obj->id}}">
                <th scope="row">{{++$i}}</th>
                <td>{{$obj->product_name}}</td>
                <td>{{$obj->quantity}}</td>
                <td>{{$obj->sale_price}}</td>
                <td>{{$obj->sale_price * $obj->quantity}}</td>
                <td><span class="cursor remove-cart-row" data-cart-id="{{$obj->id}}"><i class="fas fa-window-close"></i></span></td>
            </tr>
            @php $total += $obj->sale_price * $obj->quantity;@endphp
        @endforeach
        @endisset
        <tr style="background: beige">
            <td colspan="5">Total</td>
            <td colspan="1">{{$total}}</td>
        </tr>
        </tbody>
    </table>

</div>

@endsection