<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">ONLINE</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
            <li class="nav-item active">
                <a class="nav-link" href="{{URL::to('/')}}">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{URL::to('cart')}}">Cart</a>
            </li>
            @auth
                <li class="nav-item">
                    <a class="nav-link pointer" onclick="auth.signout()">Logout</a>
                </li>
            @else
                <li class="nav-item">
                    <a class="nav-link pointer" onclick="login()">Login</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link pointer" onclick="register()">Register</a>
                </li>

            @endauth
        </ul>
    </div>
    <span class="navbar-text">
                            USER : {{session('guest')}}
                            - Cart <span class="cartcount"></span> (<span class="carttotal"></span>)
                        </span>
</nav>