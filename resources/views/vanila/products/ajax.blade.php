<div class="col-md-4">
                    <div class="text-center">
                        @if($obj->url && is_file(public_path('uploads/products/'.$obj->url))) 
                            <img src="{{asset('public/uploads/products/'.$obj->url)}}" class="w-100" alt="...">
                        @else
                            <img src="{{asset('public/assets/img/add_image.png')}}" class="w-100" alt="...">
                        @endif
                    </div>
                </div>
                <div class="col-md-8">
                    <h3>{{$obj->product_name}}<small><br>{{$obj->tagline}}</small></h3>
                    <p>{!! $obj->summary !!}</p>
                    <select id="qty" class="form-control" style="width:100px;">
                        @php
                                echo '<option selected>1</option>';
                                for($i=2;$i<=10;$i++){
                                    echo '<option>'.$i.'</option>';
                                }
                        @endphp
                    </select><br>
                    @if(count($level1_variants)>0 || count($level2_variants)>0 || count($level3_variants)>0)
                        <div class="row" id="variant-holder">
                            <input type="hidden" id="level1_variant" value="{{$obj->level1_attribute_value_id}}">
                            <input type="hidden" id="level2_variant" value="{{$obj->level2_attribute_value_id}}">
                            <input type="hidden" id="level3_variant" value="{{$obj->level3_attribute_value_id}}">
                            @if(count($level1_variants)>0)
                                @include('vanila/products/attributes', ['variants'=>$level1_variants, 'product'=>$obj, 'level'=>1, 'available_attribute'=>$obj->available_variants['level1_available_variants'], 'selected_attribute'=>$obj->level1_attribute_value_id])
                            @endif
                            @if(count($level2_variants)>0)
                                @include('vanila/products/attributes', ['variants'=>$level2_variants, 'product'=>$obj, 'level'=>2, 'available_attribute'=>$obj->available_variants['level2_available_variants'], 'selected_attribute'=>$obj->level2_attribute_value_id])
                            @endif
                            @if(count($level3_variants)>0)
                                @include('vanila/products/attributes', ['variants'=>$level3_variants, 'product'=>$obj, 'level'=>3, 'available_attribute'=>$obj->available_variants['level3_available_variants'], 'selected_attribute'=>$obj->level3_attribute_value_id])
                            @endif

                        </div>
                    @endif
                    <button class="btn btn-primary add-to-cart-single" data-id="{{$obj->id}}">Add to cart</button>
                </div>