@extends('vanila.base')

@section('content')
    <div class="loading" style="display: none;">Loading&#8230;</div>
    <div id="ajax-loaded-content" class="row">
        @include('vanila/products/ajax')
    </div>
@endsection

@section('bottom')
<script type="text/javascript">

$(document).ready(function(){
    $(document).on('click', '.product-attribute-link', function(){

        var data = {
            _token: '{{csrf_token()}}',
            product_id: $(this).data('id'),
            attr_value_id: $(this).data('attribute-value'),
            level: $(this).data('level'),
            variant_level1: $('#variant-holder #level1_variant').val(),
            variant_level2: $('#variant-holder #level2_variant').val(),
            variant_level3: $('#variant-holder #level3_variant').val(),
        }
        $('.loading').show();
        $.post("{{url('product/variant')}}",data, function(result){
            $('#ajax-loaded-content').html(result.html);
            replace_url(result.slug);
            $('.loading').hide();
        })
    })
});

function replace_url(slug)
{
    var url = document.location.href;
    var url_arr = url.split('/');
    url_arr.pop();
    url = url_arr.join('/')+'/'+slug;
    window.history.replaceState(null, null, url);
}


</script>
@endsection