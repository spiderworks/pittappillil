@extends('admin.common.base')

@section('content')
    <div class="container-fluid">

        <div class="col-md-12" style="margin-bottom: 20px;" align="right">
            @if($obj->id)
                <span class="page-heading">EDIT Category</span>
            @else
                <span class="page-heading">Create Category</span>
            @endif
            <div >
                <div class="btn-group">
                    <a href="{{route('admin.branch.home')}}"  class="btn btn-success"><i class="fa fa-list"></i> List
                    </a>
                    @if($obj->id)
                    <a href="{{route('admin.branch.create')}}" class="btn btn-success"><i class="fa fa-pencil"></i> Create new
                    </a>
                    <button type="button" class="btn btn-success"><i class="fa fa-trash-o"></i> Delete
                    </button>
                    @endif
                </div>
            </div>
        </div>

        <div class="col-lg-12">
            <div class="card card-borderless">
                @if($obj->id)
                    {{Form::open(['url' => route('admin.branch.update'), 'method' => 'post','enctype' => 'multipart/form-data'])}}
                    <input type="hidden" name="id" value="{{encrypt($obj->id)}}">
                @else
                    {{Form::open(['url' => route('admin.branch.save'), 'method' => 'post','enctype' => 'multipart/form-data'])}}
                @endif
                @csrf

                <ul class="nav nav-tabs nav-tabs-simple d-none d-md-flex d-lg-flex d-xl-flex" role="tablist" data-init-reponsive-tabs="dropdownfx">
                    <li class="nav-item">
                        <a class="active show" data-toggle="tab" role="tab"
                           data-target="#tab1Basic"
                        href="#" aria-selected="true">Mandatory Details</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" data-toggle="tab" role="tab"
                           data-target="#tab3SEO"
                        class="" aria-selected="false">SEO</a>
                    </li>
                    {{--<li class="nav-item">
                        <a href="#" data-toggle="tab" role="tab"
                           data-target="#tab4Media"
                           class="" aria-selected="false">Media</a>
                    </li>--}}
                </ul>
                <div class="nav-tab-dropdown cs-wrapper full-width d-lg-none d-xl-none d-md-none"><div class="cs-select cs-skin-slide full-width" tabindex="0"><span class="cs-placeholder">Hello World</span><div class="cs-options"><ul><li data-option="" data-value="#tab2hellowWorld"><span>Hello World</span></li><li data-option="" data-value="#tab2FollowUs"><span>Hello Two</span></li><li data-option="" data-value="#tab2Inspire"><span>Hello Three</span></li></ul></div><select class="cs-select cs-skin-slide full-width" data-init-plugin="cs-select"><option value="#tab2hellowWorld" selected="">Hello World</option><option value="#tab2FollowUs">Hello Two</option><option value="#tab2Inspire">Hello Three</option></select><div class="cs-backdrop"></div></div></div>
                <div class="tab-content">
                    <div class="tab-pane active show" id="tab1Basic">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row column-seperation padding-5">
                                    <div class="form-group form-group-default required">
                                        <label>Branch name</label>
                                        {{ Form::text("branch_name", $obj->branch_name, array('class'=>'form-control', 'id' => 'branch_name','required' => true)) }}

                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row column-seperation padding-5">
                                    <div class="form-group form-group-default required">
                                        <label>Browser title</label>
                                        {{ Form::text("browser_title", $obj->browser_title, array('class'=>'form-control', 'id' => 'browser_title')) }}

                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row column-seperation padding-5">
                                    <div class="form-group form-group-default required">
                                        <label>Page heading</label>
                                        {{ Form::text("page_heading", $obj->page_heading, array('class'=>'form-control', 'id' => 'page_heading')) }}

                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row column-seperation padding-5">
                                    <div class="form-group form-group-default required">
                                        <label>Branch address</label>
                                        {{ Form::textarea("address", $obj->address, array('class'=>'form-control', 'id' => 'address')) }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row column-seperation padding-5">
                                    <div class="form-group form-group-default required">
                                        <label>Branch description</label>
                                        {{ Form::textarea("description", $obj->description, array('class'=>'form-control', 'id' => 'description')) }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row column-seperation padding-5">
                                    <div class="form-group form-group-default required">
                                        <label class="">Branch slug (for url)</label>
                                        {{ Form::text("slug", $obj->slug, array('class'=>'form-control', 'id' => 'slug')) }}

                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="row column-seperation padding-5">
                                    <div class="form-group form-group-default required">
                                        <label class="">Phone number</label>
                                        {{ Form::text("phone", $obj->phone, array('class'=>'form-control', 'id' => 'phone')) }}

                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="row column-seperation padding-5">
                                    <div class="form-group form-group-default required">
                                        <label class="">Website</label>
                                        {{ Form::text("website", $obj->website, array('class'=>'form-control', 'id' => 'website')) }}

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="tab-pane" id="tab3SEO">
                        <div class="row">

                            <div class="col-md-6">
                                <div class="row column-seperation padding-5">
                                    <div class="form-group form-group-default">
                                        <label>Meta title</label>
                                        {{ Form::text("browser_title", $obj->browser_title, array('class'=>'form-control', 'id' => 'browser_title')) }}

                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row column-seperation padding-5">
                                    <div class="form-group form-group-default">
                                        <label class="">Meta Keywords</label>
                                        {{ Form::text("meta_keywords", $obj->meta_keywords, array('class'=>'form-control', 'id' => 'meta_keywords')) }}

                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row column-seperation padding-5">
                                    <div class="form-group form-group-default">
                                        <label class="">Meta description</label>
                                        <input type="text" class="form-control" name="meta_description">
                                        {{ Form::text("meta_description", $obj->meta_description, array('class'=>'form-control', 'id' => 'meta_description')) }}

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{--<div class="tab-pane" id="tab4Media">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="row column-seperation padding-5">
                                    <div class="form-group form-group-default">
                                        <label>Banner Image</label>
                                        <input type="file" class="form-control" name="banner">
                                    </div>
                                </div>
                                @isset($obj->banner)
                                    <img src="{{URL::asset($obj->banner->url)}}" width="100%" alt="">
                                @endisset
                            </div>
                            <div class="col-md-4">
                                <div class="row column-seperation padding-5">
                                    <div class="form-group form-group-default">
                                        <label>Primary image</label>
                                        <input type="file" class="form-control" name="primary">
                                    </div>
                                </div>
                                @isset($obj->primary)
                                    <img src="{{URL::asset($obj->primary->url)}}" width="100%" alt="">
                                @endisset
                            </div>
                        </div>
                    </div>--}}
                    <div class="row">
                        <div class="col-md-12" align="right">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
@endsection
@section('bottom')
    @parent
@endsection