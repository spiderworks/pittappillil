@extends('admin.common.base')

@section('head')
@endsection

@section('content')
    <div class="container-fluid">
        <div class="col-md-12 mb-20"  align="right" style="margin-bottom: 20px; ">
          <span class="page-heading">@if($obj->id)Edit User @else Add new User @endif</span>
          <div >
              <div class="btn-group">
                  <a href="{{url('admin/users')}}" class="btn btn-success"><i class="fa fa-list"></i> List Users</a>
                  @if($obj->id)
                    <a href="{{url('admin/users/create')}}" class="btn btn-success"><i class="fa fa-pencil"></i> Create new</a>
                    <a href="{{url('admin/users/destroy', [encrypt($obj->id)])}}" class="btn btn-success btn-warning-popup" data-message="Are you sure to delete?  Associated data will be removed if it is deleted." data-redirect-url="{{url('admin/users')}}"><i class="fa fa-trash"></i> Delete</a>
                  @endif
              </div>
          </div>
        </div>
        <div class="col-lg-12">
            <div class="card card-borderless">
                @if($obj->id)
                    {{Form::open(['url' => route('admin.offers.update'), 'method' => 'post','enctype' => 'multipart/form-data', 'id'=>'OfferFrm'])}}
                    <input type="hidden" name="id" value="{{encrypt($obj->id)}}" id="inputId">
                @else
                    {{Form::open(['url' => route('admin.offers.store'), 'method' => 'post','enctype' => 'multipart/form-data', 'id'=>'OfferFrm'])}}
                @endif
                <ul class="nav nav-tabs nav-tabs-simple d-none d-md-flex d-lg-flex d-xl-flex" role="tablist" data-init-reponsive-tabs="dropdownfx">
                    <li class="nav-item">
                        <a class="active show" data-toggle="tab" role="tab"
                           data-target="#tab1Basic"
                        href="#" aria-selected="true">Basic Details</a>
                    </li>
                    <li class="nav-item">
                        <a data-toggle="tab" role="tab"
                           data-target="#tab2Basic"
                        href="#" aria-selected="true">SEO</a>
                    </li>
                    <li class="nav-item">
                        <a data-toggle="tab" role="tab"
                           data-target="#tab3Basic"
                        href="#" aria-selected="true">Products</a>
                    </li>
                </ul>
                
                <div class="tab-content">
                    <div class="tab-pane active show" id="tab1Basic">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row column-seperation padding-5">
                                    <div class="form-group form-group-default form-group-default-select2 required">
                                        <label>Status</label>
                                        {!! Form::select('is_active', array('1'=>'Enabled', '0'=>'Disabled'), ($obj->id && $obj->banned_at)?0:1, array('class'=>'full-width select2-dropdown', 'id'=>'inputStatus')); !!}

                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row column-seperation padding-5">
                                    <div class="form-group form-group-default required">
                                        <label>Offer Name</label>
                                        {{ Form::text("offer_name", $obj->offer_name, array('class'=>'form-control', 'id' => 'offer_name','required' => true)) }}

                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row column-seperation padding-5">
                                    <div class="form-group form-group-default form-group-default-select2 required">
                                        <label>Type</label>
                                        {!! Form::select('type', array(''=>'-- Select --', 'Price'=>'Price', 'Combo'=>'Combo', 'Free'=>'Free'), ($obj->id && $obj->banned_at)?0:1, array('class'=>'full-width select2-dropdown', 'id'=>'inputType')); !!}

                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row column-seperation padding-5">
                                    <div class="form-group form-group-default input-group required">
                                      <div class="form-input-group">
                                        <label>Offer Start Date</label>
                                        {{ Form::text("validity_start_date", $obj->validity_start_date, array('class'=>'form-control datepicker', 'id' => 'validity_start_date')) }}
                                      </div>
                                      <div class="input-group-append ">
                                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                      </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row column-seperation padding-5">
                                    <div class="form-group form-group-default input-group required">
                                      <div class="form-input-group">
                                        <label class="">Offer End Date</label>
                                        {!! Form::text('validity_end_date',$obj->validity_end_date, array('class'=>'form-control datepicker', 'id'=>'validity_end_date')); !!}
                                      </div>
                                      <div class="input-group-append ">
                                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                      </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab2Basic">
                        <div class="row">
                            <div class="col-md-6">
                              <div class="row column-seperation padding-5">
                                  <div class="form-group form-group-default">
                                      <label>Browser Title</label>
                                      {{ Form::text("browser_title", $obj->browser_title, array('class'=>'form-control', 'id' => 'browser_title')) }}
                                  </div>
                              </div>
                          </div>
                          <div class="col-md-6">
                              <div class="row column-seperation padding-5">
                                  <div class="form-group form-group-default">
                                      <label>Meta Tags</label>
                                      {{ Form::textarea("meta_keywords", $obj->meta_keywords, array('class'=>'form-control', 'id' => 'meta_keywords')) }}
                                  </div>
                              </div>
                          </div>
                          <div class="col-md-6">
                              <div class="row column-seperation padding-5">
                                  <div class="form-group form-group-default">
                                      <label>Meta Description</label>
                                      {{ Form::textarea("meta_description", $obj->meta_description, array('class'=>'form-control', 'id' => 'meta_description')) }}
                                  </div>
                              </div>
                          </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab3Basic">
                      <div class="row offer-wrapper">
                          <div class="col-md-6">
                            <div class="card card-default">
                              <div class="card-header">
                              Products
                              <div class="pull-right">
                                <a href="javascript:void(0)" class="btn btn-sm btn-primary" id="source-btn">Move to Offers >></a>
                              </div>
                              <div class="row">
                                <a href="javascript:void(0);" class="pull-right"><i class="fa fa-filter"></i></a>
                              </div>
                              </div>
                              <div class="card-body">
                                <ul class="list-group" id="source">
                                  @if(count($products)>0)
                                    @foreach($products as $product)
                                      <li class="list-group-item" id="{{$product->id}}">
                                        <label>{{$product->product_name}}</label>
                                      </li>
                                    @endforeach
                                  @endif
                                </ul>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="card card-default">
                              <div class="card-header">
                                Offers
                                <div class="pull-right">
                                  <a href="javascript:void(0)" class="btn btn-sm btn-primary" id="destination-btn"><< Return to Products</a>
                                </div>
                              </div>
                              <div class="card-body">
                                <ul class="list-group" id="destination">

                                </ul>
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12" align="right">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
@endsection
@section('bottom')
    <script>
      

      $(document).ready(function(){
        $(document).on('click', '#source .list-group-item', function(){
          if($(this).hasClass('selected'))
            $(this).removeClass('selected');
          else
            $(this).addClass('selected');
        });

        $(document).on('click', '#destination .list-group-item', function(){
            if($(this).hasClass('deselected'))
              $(this).removeClass('deselected');
            else
              $(this).addClass('deselected');
        });

        $(document).on('click', '#source-btn', function(){
          $('.selected').each( function(){
            $(this).removeClass('selected');
            $(this).detach().appendTo('#destination');
          })
        });

        $(document).on('click', '#destination-btn', function(){
          $('.deselected').each( function(){
            $(this).removeClass('deselected');
            $(this).detach().appendTo('#source');
          })
        });

       var validator = $('#UserFrm').validate({
          ignore: [],
          invalidHandler: function() {
            if(validator.numberOfInvalids())
            {
                if($('.alert-error').length>0)
                    $('.alert-error').remove();
                  var html = '<div class="alert alert-error alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Error:</strong>Oops! look like you have missed some important fields, please check all tabs.</div>';
                  $( html ).insertBefore( ".page-wrapper" );
            }
          },
          rules: {
            first_name: "required",
            last_name: "required",
            email: {
              required: true,
              email: true,
              remote: {
                  url: "{{url('validation/user')}}",
                  data: {
                    id: function() {
                      return $( "#inputId" ).val();
                  }
                }
              }
            },
            password: {
              required: function(element){
                  return $("#inputId").length<=0;
              }
            },
            password_confirmation: {
              equalTo: "#password",
            },
            phone_number: {
              digits : true,
              maxlength : 10,
            },
          },
          messages: {
            first_name: "First name cannot be blank",
            last_name: "Last name cannot be blank",
            email: {
              required: "Email address cannot be blank",
              remote: "Email is already in use",
            },
            password: "Password cannot be blank",
          },
        });
      });
    </script>
    @parent
@endsection