@extends('admin.common.base')

@section('content')
    <div class="container-fluid bg-white">
        <div class="container">
            <!-- START card -->
            <div class="card card-transparent">
                <div class="card-header ">
                    <div class="card-title">All Categories
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-hover demo-table-search table-responsive-block" id="datatable"
                           data-datatable-ajax-url="{{ url('admin/category/home') }}" >
                        <thead id="column-search">
                        <tr>
                            <th class="table-width-10">ID</th>
                            <th class="table-width-120">Slug</th>
                            <th class="table-width-120">Category name</th>
                            <th class="table-width-120">Browser title</th>
                            <th class="table-width-120">Meta Keywords</th>
                            <th class="nosort nosearch table-width-10">Delete</th>
                            <th class="nosort nosearch table-width-10">Edit</th>
                        </tr>

                        <tr>
                            <th class="table-width-10 nosort nosearch"></th>
                            <th class="table-width-10 searchable-input">Slug</th>
                            <th class="table-width-120 searchable-input">Category name</th>
                            <th class="table-width-120 searchable-input">Browser title</th>
                            <th class="table-width-120 searchable-input">Meta Keywords</th>
                            <th class="nosort nosearch table-width-10"></th>
                            <th class="nosort nosearch table-width-10"></th>
                        </tr>

                        </thead>

                        <tbody>
                        </tbody>

                        {{--<tfoot id="column-search">
                        <tr>
                            <th class="table-width-10 searchable-input">ID</th>
                            <th class="table-width-10 searchable-input">Slug</th>
                            <th class="table-width-120 searchable-input">Category name</th>
                            <th class="table-width-120 searchable-input">Browser title</th>
                            <th class="table-width-120 searchable-input">Meta Keywords</th>
                        </tr>
                        </tfoot>--}}
                    </table>
                </div>
            </div>
            <!-- END card -->
        </div>
    </div>
@endsection
    @section('bottom')

    @parent
@endsection