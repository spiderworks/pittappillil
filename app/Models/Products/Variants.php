<?php

namespace App\Models\Products;

use App\Models\BaseModel, App\Models\ValidationTrait, DB;

class Variants extends BaseModel
{
	use ValidationTrait {
        ValidationTrait::validate as private parent_validate;
    }
    
    public function __construct() {
        
        parent::__construct();
        $this->__validationConstruct();
    }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'product_variants';


    protected $fillable = array('products_id', 'name', 'slug', 'image_id', 'level1_attribute_value_id', 'level2_attribute_value_id', 'level3_attribute_value_id', 'is_default', 'sku', 'retail_price', 'sale_price', 'landing_price', 'quantity', 'short_description');

    protected $dates = ['created_at','updated_at'];


    protected function setRules() {

        $this->val_rules = array(
            'name' => 'required|max:250',
            'slug' => 'required|alpha_dash|unique:product_variants,slug,ignoreId',
            'sku' => 'required|max:250',
            'retail_price' => 'required',
            'sale_price' => 'required',
            'short_description' => 'max:1000',
        );
    }

    protected function setAttributes() {
        $this->val_attributes = array(
        );
    }

    public function validate($data = null, $ignoreId = 'NULL') {
        $ignore_array = ['slug'];
        foreach($ignore_array as $ignore){ 
            $this->val_rules[$ignore] = str_replace('ignoreId', $ignoreId, $this->val_rules[$ignore]);
        }
        return $this->parent_validate($data);
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Products', 'products_id');
    }

    public function media()
    {
        return $this->belongsTo('App\Models\Media', 'image_id');
    }

}