<?php

namespace App\Models\Category;

use App\Models\BaseModel, App\Models\ValidationTrait, DB;

class CategoryAttributeGroups extends BaseModel
{
	use ValidationTrait {
        ValidationTrait::validate as private parent_validate;
    }
    
    public function __construct() {
        
        parent::__construct();
        $this->__validationConstruct();
    }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'product_cateory_attribute_groups';


    protected $fillable = array('group_name');

    protected $dates = ['created_at','updated_at'];


    protected function setRules() {

        $this->val_rules = array(
        );
    }

    protected function setAttributes() {
        $this->val_attributes = array(
        );
    }

    public static function listForSelect($default = '--- Select a Group ---') {
        $list[''] = $default;
        foreach (static::orderBy('group_name', 'ASC')->get() as $group) {
            $list[$group->id] = $group->group_name;
        }
        return $list;
    }

}
