<?php

namespace App\Models;

use App\Models\BaseModel, App\Models\ValidationTrait, DB;

class Offers extends BaseModel
{
	use ValidationTrait {
        ValidationTrait::validate as private parent_validate;
    }
    
    public function __construct() {
        
        parent::__construct();
        $this->__validationConstruct();
    }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'offers';


    protected $fillable = array('offer_name', 'type', 'validity_start_date', 'validity_end_date', 'is_active', 'browser_title', 'meta_keywords', 'meta_description');

    protected $dates = ['created_at','updated_at'];

    public $uploadPath = array(
        
    );


    protected function setRules() {

        $this->val_rules = array(
            'offer_name'=> 'required',
            'type' => 'required',
            'validity_start_date' => 'required',
            'validity_end_date' => 'required',
        );
    }

    protected function setAttributes() {
        $this->val_attributes = array(
        );
    }

    public function validate($data = null, $ignoreId = 'NULL') {
        
    }

}