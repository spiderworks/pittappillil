<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    protected $table = 'branches';
    protected $fillable = ['phone','branch_name','browser_title','address','description','email','lattitude','longitude','slug','website','meta_description','meta_keywords','page_heading'];

    use ValidationTrait {
        ValidationTrait::validate as private parent_validate;
    }


    public function __construct() {

        parent::__construct();
        $this->__validationConstruct();
    }

    protected function setRules() {
        $this->val_rules = [
            'slug' => 'required|alpha_dash|unique:brands,slug,ignoreId',
            'branch_name' => 'required|max:150',
            'page_heading' => 'nullable',
            'browser_title' => 'nullable',
            'meta_keywords' => 'nullable',
            'meta_description' => 'nullable',
        ];
    }

    protected function setAttributes() {
        $this->val_attributes = [
            'slug' => 'vendor slug',
            'branch_name' => 'branch name',
            'page_heading' => 'page heading',
            'browser_title' => 'browser title',
            'meta_keywords' => 'meta keywords',
            'meta_description' => 'meta description'
        ];
    }

    public function validate($data = null, $ignoreId = 'NULL') {
        $ignore_array = ['slug'];
        foreach($ignore_array as $ignore){
            $this->val_rules[$ignore] = str_replace('ignoreId', $ignoreId, $this->val_rules[$ignore]);
        }
        return $this->parent_validate($data);
    }
}