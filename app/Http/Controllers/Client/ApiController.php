<?php

namespace App\Http\Controllers\Client;

use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Products;
use DB;

class ApiController extends Controller
{
    public function productlist($type){

        $product = DB::table('products as p')
            ->select('p.id','p.product_name','f.url','p.mrp','p.sale_price')
            ->join('files as f','p.default_image_id','=','f.id');
        switch ($type){
            case 'newproducts':
                $data['data'] = $product->where('p.is_new','1')->limit(8)->get();
                return json_encode($data);
            case 'topseller':
                $data['data'] = $product->where('p.is_top_seller','1')->limit(8)->get();
                return json_encode($data);
        }
        return Products::all();
    }
}
