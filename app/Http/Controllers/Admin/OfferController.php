<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use App\Http\Controllers\Admin\ResourceTrait;
use Input, Request, View, Redirect, DB, Datatables, Sentinel, Mail, Validator, Image;
use Activation as Act;
use App\Models\Offers;
use App\Models\Products;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request as Reqst;

class OfferController extends BaseController
{
    use ResourceTrait;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->model = new Offers;

        $this->route .= '.offers';
        $this->views .= '.offers';
        $this->url = "admin/offers/";

        $this->resourceConstruct();

    }

    protected function getCollection() {
        return $this->model->select('id', 'offer_name', 'type', 'validity_start_date', 'validity_end_date', 'is_active', 'updated_at');
    }

    protected function setDTData($collection) {
        $route = $this->route;
        return $this->initDTData($collection)
            ->editColumn('updated_at', function($obj) { return date('m/d/Y H:i:s', strtotime($obj->updated_at)); })
            ->editColumn('is_active', function($obj) use ($route) {
                $id =  $obj->id;
                if($obj->is_active)
                    return '<a href="' . url('admin/offers/change-status', [$id]) . '" class="btn btn-danger btn-sm btn-warning-popup" data-message="Are you sure, want to disable this offer?"><i class="fa fa-times-circle"></i></a>';
                else
                   return '<a href="' . url('admin/offers/change-status', [$id]) . '" class="btn btn-success btn-sm btn-warning-popup" data-message="Are you sure, want to enable this offer?"><i class="fa fa-check-circle"></i></a>'; 
            })
            ->editColumn('validity_start_date', function($obj) { return date('m/d/Y', strtotime($obj->validity_start_date)); })
            ->editColumn('validity_end_date', function($obj) { return date('m/d/Y', strtotime($obj->validity_end_date)); })
            ->rawColumns(['is_active', 'action_edit', 'action_delete']);
    }

    public function create()
    {
        $products = DB::table('products')->select('products.id', 'products.product_name')->where('is_active', 1)->where('is_completed', 1)->whereNull('products.deleted_at')->paginate(10);
        return view($this->views . '.form')->with('obj', $this->model)->with('products', $products);
    }

    public function edit($id) {
        $id = decrypt($id);
        if($obj = $this->model->find($id)){
            return view($this->views . '.form')->with('obj', $obj);
        } else {
            return $this->redirect('notfound');
        }
    }

    public function store(Reqst $r)
    {
        $data = $r->all();
        //print_r($data);exit;
        $validator = Validator::make($data, [
            'first_name' => 'required|max:250',
            'last_name' => 'required|max:250',
            'email' => 'required|email|unique:users,email|max:250',
            'password' => 'required|same:password_confirmation',
            'phone_number' => 'max:10',
            'pin_code' => 'max:10',
            'address' => 'max:500',

        ]);
        if ($validator->fails()){
            return Redirect::back()->withInput()->withErrors($validator->errors()->all());
        }
        else
        {
            $data['username'] = $data['email'];
            $data['password'] = Hash::make($data['password']);
            if($data['status'] == 0)
                $data['banned_at'] = date('Y-m-d H:i:s');
            else
                $data['banned_at'] = null;

            $user = User::create($data);
            $user->assignRole(['1']);

            return Redirect::to(url('admin/users/edit', array('id'=>encrypt($user->id))))->withSuccess('User details successfully saved!'); 
        } 
    }

    public function update(Reqst $r)
    {
        $data = $r->all();
        $id = decrypt($data['id']);
        $validator = Validator::make($data, [
            'first_name' => 'required|max:250',
            'last_name' => 'required|max:250',
            'email' => 'required|email|max:250|unique:users,email,'.$id,
            'password' => 'nullable|same:password_confirmation',
            'phone_number' => 'max:10',
            'pin_code' => 'max:10',
            'address' => 'max:500',
        ]);
        if ($validator->fails()){
            return Redirect::back()->withInput()->withErrors($validator->errors()->all());
        }
        else
        {
            if($obj = $this->model->find($id)){
                if($data['password'] != '')
                    $data['password'] = Hash::make($data['password']);
                else
                    unset($data['password']);
                if($data['status'] == 0)
                    $data['banned_at'] = date('Y-m-d H:i:s');
                else
                    $data['banned_at'] = null;

                $obj->update($data);

                return Redirect::to(url('admin/users/edit', array('id'=>encrypt($obj->id))))->withSuccess('User details successfully updated!');
            } else {
                return Redirect::back()
                        ->withErrors("Ooops..Something wrong happend.Please try again.") // send back all errors to the login form
                        ->withInput(Input::all());
            }
        }
    }

    public function destroy($id) {
        $id = decrypt($id);
        $obj = $this->model->find($id);
        if ($obj) {
            $obj->delete();
            return response()->json(['success'=>'User successfully deleted']);
        }
        return response()->json(['error'=>'Oops!! something went wrong...Please try again.']);
    }

    public function changeStatus($id)
    {
        //$id = decrypt($id);
        $obj = $this->model->find($id);
        if ($obj) {
            if($obj->banned_at)
            {
                $obj->banned_at = null;
                $message = 'enabled';
            }
            else
            {
                $obj->banned_at = date('Y-m-d H:i:s');
                $message = 'disabled';
            }
            $obj->save();
            return response()->json(['success'=>'User successfully '.$message]);
        }
        return response()->json(['error'=>'Oops!! something went wrong...Please try again.']);
    }

}
