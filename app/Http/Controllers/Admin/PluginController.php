<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use App\Models\File,URL;

class PluginController extends BaseController
{
    public function summernote_image_upload(Request $request){
        $image = $this->upload_file($request->file('file'),'summernote','','image from summernote');
        if($image){
            $img = File::find($image['data'])->url;
            echo URL::asset($img);
        }

    }
}
