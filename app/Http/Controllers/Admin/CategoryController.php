<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Support\Facades\Input;
use View,Redirect;
use App\Http\Controllers\Admin\Common\ImageController as Image;

class CategoryController extends BaseController
{
    use ResourceTrait;

    public function __construct()
    {
        parent::__construct();

        $this->model = new Category;
        $this->route .= '.category';
        $this->views .= '.category';
        $this->url = "admin/category/";
        $this->resourceConstruct();

    }
    protected function getCollection() {
        return $this->model->select('id', 'slug', 'category_name', 'browser_title', 'meta_keywords');
    }

    public function home(Request $request, $parent=null){
        if ($request->ajax()) {
            $collection = $this->getCollection();
            $parent_id = null;
            if($parent)
                $parent_id = $parent;
//            $collection->where('categories.parent_category_id', '=', $parent_id);
            $route = $this->route;
            return $this->setDTData($collection)->make(true);
        } else {
            $parent_data = null;
            if($parent)
                $parent_data = $this->model->find($parent);
            return view::make($this->views . '.home', ['parent'=>$parent, 'parent_data'=>$parent_data]);
        }
    }



    protected function setDTData($collection) {
        $route = $this->route;
        return $this->initDTData($collection)
            ->rawColumns(['action_edit', 'action_delete']);
    }

    public function category_select2(Request $r){
        return $this->select2('slug',Input::get('q'),'category_name');
    }

    public function create(){
        $obj = $this->model;
        return view($this->views.'.form',['obj'=>$obj]);
    }

    public function edit($id){
        if($obj = $this->model->find(decrypt($id))){
            return view($this->views . '.form',['obj' => $obj]);
        } else {
            return $this->redirect('notfound');
        }
    }

    public function destroy($id)
    {
    }

    public function save(Request $request){
        $this->model->validate();
        $data = Input::all();
        if($request->file('banner')){
            $upload = $this->uploadFile($request->file('banner'),'uploads/category/banner');
            if($upload['success']) {
                $result = $this->saveMedia($upload);
                $data['banner_image']=$result['id'];
            }
        }
        if($request->file('primary')){
            $upload = $this->uploadFile($request->file('primary'),'uploads/category/primary');
            if($upload['success']) {
                $result = $this->saveMedia($upload);
                $data['thumbnail_image']=$result['id'];
            }
        }
        $this->model->fill($data);
        $this->model->save();
        

        return redirect(route($this->route.'.edit', ['id' => encrypt($this->model->id)]))
            ->withSuccess("Category created succesfully");
        // send back all errors to the login form;
    }

    public function update(Request $request){
        $this->model->validate(Input::all(), decrypt($request->id));
        $data = Input::all();
        if($request->file('banner')){
            $upload = $this->upload_file($request->file('banner'),'category/banner',$request->category_name);
            if($upload['status']){$data['banner_image']=$upload['data'];}
        }
        if($request->file('primary')){
            $upload = $this->upload_file($request->file('primary'),'category/primary',$request->category_name);
            if($upload['status']){$data['thumbnail_image']=$upload['data'];}
        }
        if($obj = $this->model->find(decrypt($request->id))) {
            $obj->update($data);
            $obj->save();
        }

        return Redirect::back()
            ->withSuccess("Category updated succesfully") // send back all errors to the login form
            ->withInput(Input::all());
    }


}
