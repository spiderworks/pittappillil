<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class PluginsController extends Controller
{
    public function select2_countries(Request $r){
        $categories = DB::table('countries')->where('name', 'like', $r->q.'%')->orderBy('name')
            ->get();
        $json = [];
        foreach($categories as $c){
            $json[] = ['id'=>$c->id, 'text'=>$c->name];
        }
        return \Response::json($json);
    }

    public function select2_states(Request $r){
        $json = [];
        $country_id = $r->country_id;
        if($country_id)
        {
            $categories = DB::table('states')->where('name', 'like', $r->q.'%')->where('CountryID', $country_id)->orderBy('name')
                ->get();
            foreach($categories as $c){
                $json[] = ['id'=>$c->id, 'text'=>$c->name];
            }
        }
        return \Response::json($json);
    }

    public function select2_categories(Request $r){
        $categories = DB::table('categories')->where('category_name', 'like', $r->q.'%')->orderBy('category_name')
            ->get();
        $json = [];
        foreach($categories as $c){
            $json[] = ['id'=>$c->id, 'text'=>$c->category_name];
        }
        return \Response::json($json);
    }

    public function select2_brands(Request $r){
        $brands = DB::table('brands')->where('brand_name', 'like', $r->q.'%')->orderBy('brand_name')
            ->get();
        $json = [];
        foreach($brands as $c){
            $json[] = ['id'=>$c->id, 'text'=>$c->brand_name];
        }
        return \Response::json($json);
    }

    public function select2_venders(Request $r){
        $venders = DB::table('vendors')->where('vendor_name', 'like', $r->q.'%')->orderBy('vendor_name')
            ->get();
        $json = [];
        foreach($venders as $c){
            $json[] = ['id'=>$c->id, 'text'=>$c->vendor_name];
        }
        return \Response::json($json);
    }

    public function unique_user(Request $r)
    {
         $id = $r->id;
         $email = $r->email;
         
         $where = "email='".$email."'";
         if($id)
            $where .= " AND id != ".decrypt($id);
         $user = DB::table('users')
                    ->whereRaw($where)
                    ->get();
         
         if (count($user)>0) {  
             echo "false";
         } else {  
             echo "true";
         }
    }

    public function unique_attribute_slug(Request $r)
    {
        $id = $r->id;
        $attribute_slug = $r->attribute_slug;
         
         $where = "attribute_slug='".$attribute_slug."'";
         if($id)
            $where .= " AND id != ".decrypt($id);
         $result = DB::table('product_cateory_attributes')
                    ->whereRaw($where)
                    ->get();
         
         if (count($result)>0) {  
             echo "false";
         } else {  
             echo "true";
         }
    }

    public function unique_attribute_value_slug(Request $r)
    {
        $id = $r->id;
        $value_slug = $r->value_slug;
         
         $where = "value_slug='".$value_slug."'";
         if($id)
            $where .= " AND id != ".decrypt($id);
         $result = DB::table('product_cateory_attribute_values')
                    ->whereRaw($where)
                    ->get();
         
         if (count($result)>0) {  
             echo "false";
         } else {  
             echo "true";
         }
    }

    public function unique_product_slug(Request $r)
    {
        $id = $r->id;
        $slug = $r->slug;
         
         $where = "slug='".$slug."'";
         if($id)
            $where .= " AND id != ".decrypt($id);
         $result = DB::table('products')
                    ->whereRaw($where)
                    ->get();
         
         if (count($result)>0) {  
             echo "false";
         } else {  
             echo "true";
         }
    }

    public function unique_product_variant_slug(Request $r)
    {
        $id = $r->id;
        $slug = $r->slug;
         
         $where = "slug='".$slug."'";
         if($id)
            $where .= " AND id != ".decrypt($id);
         $result = DB::table('product_variants')
                    ->whereRaw($where)
                    ->get();
         
         if (count($result)>0) {  
             echo "false";
         } else {  
             echo "true";
         }
    }
}
