-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Aug 22, 2019 at 04:55 AM
-- Server version: 5.7.26
-- PHP Version: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pittappillil`
--

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

DROP TABLE IF EXISTS `address`;
CREATE TABLE IF NOT EXISTS `address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL,
  `mobile` varchar(100) NOT NULL,
  `pincode` int(11) NOT NULL,
  `state` varchar(200) NOT NULL,
  `city` varchar(200) NOT NULL,
  `address` text NOT NULL,
  `landmark` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `address`
--

INSERT INTO `address` (`id`, `name`, `user_id`, `mobile`, `pincode`, `state`, `city`, `address`, `landmark`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Akhil', 1, '9496607954', 680007, 'Karnataka', 'Kerala', 'aaaaaaaaaaaa', 'Near vattapinni temple', '2019-05-15 01:58:03', '2019-05-15 01:58:03', NULL),
(2, 'Akhil', 1, '9496607954', 680007, 'Jharkhand', 'Kerala', 'aaaaaaaaaaaa', 'Near vattapinni temple', '2019-05-15 03:01:41', '2019-05-15 03:01:41', NULL),
(3, 'Akhil', 1, '9496607954', 680007, 'Lakshadweep', 'Thrissur', 'aaaaaaaaaaaa', 'Near vattapinni temple', '2019-05-15 03:57:01', '2019-05-15 03:57:01', NULL),
(4, 'Akhil', 1, '9496607954', 680007, 'Kerala', 'Kerala', 'aaaaaaaaaaaa', 'Near vattapinni temple', '2019-05-15 04:41:45', '2019-05-15 04:41:45', NULL),
(5, 'Akhil', 1, '9496607954', 680007, 'Maharashtra', 'Kerala', 'aaaaaaaaaaaa', 'Near vattapinni temple', '2019-05-15 07:07:02', '2019-05-15 07:07:02', NULL),
(6, 'Akhil', 1, '9496607954', 680007, 'Jammu and Kashmir', 'Thrissur', 'aaaaaaaaaaaa', 'Near vattapinni temple', '2019-05-15 07:07:43', '2019-05-15 07:07:43', NULL),
(7, 'Akhil Joy', 6, '9496607954', 680007, 'Kerala', 'Thrissur', 'MANGALAM house nedupuzha p.o.', 'Vattapinni', '2019-05-17 06:52:25', '2019-05-17 06:52:25', NULL),
(8, 'Akhil', 1, '9496607954', 680007, 'Lakshadweep', 'Kerala', 'aaaaaaaaaaaa', 'Near vattapinni temple', '2019-05-18 05:29:54', '2019-05-18 05:29:54', NULL),
(9, 'Sudeep s', 1, '9562543210', 682030, 'Kerala', 'Kakkanadu', 'Spiderworks technologies pvt ltd, 219 mavelipuram zone 2', 'Near Adam star', '2019-05-18 05:33:09', '2019-05-18 05:33:09', NULL),
(10, 'Akhil', 1, '9496607954', 682030, 'Kerala', 'Kakkanadu', 'Spiderworks technologies pvt ltd, 219 mavelipuram zone 2', 'Near Adam star', '2019-05-18 05:33:54', '2019-05-18 05:33:54', NULL),
(11, 'akhil', 1, '9496607954', 53453543, 'Kerala', 'dsfdsfds', 'dfsf', 'sdfdsfsf', '2019-08-12 11:01:59', '2019-08-12 11:01:59', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

DROP TABLE IF EXISTS `cart`;
CREATE TABLE IF NOT EXISTS `cart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`id`, `user_id`, `product_id`, `quantity`, `created_at`, `updated_at`, `deleted_at`) VALUES
(7, 211908100124493, 101, 1, '2019-08-21 05:17:14', '2019-08-21 05:17:14', NULL),
(8, 221908043241779, 92, 1, '2019-08-21 23:03:05', '2019-08-21 23:03:05', NULL),
(9, 221908043241779, 93, 2, '2019-08-21 23:03:07', '2019-08-21 23:03:08', NULL),
(10, 221908043241779, 94, 1, '2019-08-21 23:03:09', '2019-08-21 23:03:09', NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
