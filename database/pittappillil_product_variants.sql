-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Aug 22, 2019 at 04:50 AM
-- Server version: 5.7.26
-- PHP Version: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pittappillil`
--

-- --------------------------------------------------------

--
-- Table structure for table `media_library`
--

DROP TABLE IF EXISTS `media_library`;
CREATE TABLE IF NOT EXISTS `media_library` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_name` text NOT NULL,
  `file_path` varchar(250) NOT NULL,
  `thumb_file_path` varchar(250) NOT NULL,
  `file_type` varchar(100) NOT NULL,
  `file_size` varchar(100) NOT NULL,
  `dimensions` varchar(50) DEFAULT NULL,
  `media_type` varchar(120) NOT NULL DEFAULT 'Image',
  `title` varchar(250) DEFAULT NULL,
  `description` mediumtext,
  `alt_text` varchar(250) DEFAULT NULL,
  `related_type` varchar(20) DEFAULT NULL,
  `related_id` int(11) DEFAULT NULL,
  `created_by` bigint(20) NOT NULL,
  `updated_by` bigint(20) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `media_library`
--

INSERT INTO `media_library` (`id`, `file_name`, `file_path`, `thumb_file_path`, `file_type`, `file_size`, `dimensions`, `media_type`, `title`, `description`, `alt_text`, `related_type`, `related_id`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'redminote7back-379x800-15598073112225d5d357ccabb5.jpeg', 'uploads/products/redminote7back-379x800-15598073112225d5d357ccabb5.jpeg', 'uploads/thumbnails/redminote7back-379x800-15598073112225d5d357ccabb5.jpeg', 'image/webp', '1702', '199 X 420', 'Image', NULL, NULL, NULL, 'Products', 3, 1, 1, '2019-08-21 12:13:49', '2019-08-21 12:13:49', NULL),
(2, '1551344291_635_redmi_note_75d5d357cc6ec5.jpg', 'uploads/products/1551344291_635_redmi_note_75d5d357cc6ec5.jpg', 'uploads/thumbnails/1551344291_635_redmi_note_75d5d357cc6ec5.jpg', 'image/webp', '19514', '560 X 420', 'Image', NULL, NULL, NULL, 'Products', 3, 1, 1, '2019-08-21 12:13:49', '2019-08-21 12:13:49', NULL),
(3, '1559042948_635_redmi_k205d5d357cc6ec5.jpg', 'uploads/products/1559042948_635_redmi_k205d5d357cc6ec5.jpg', 'uploads/thumbnails/1559042948_635_redmi_k205d5d357cc6ec5.jpg', 'image/webp', '9018', '560 X 420', 'Image', NULL, NULL, NULL, 'Products', 3, 1, 1, '2019-08-21 12:13:49', '2019-08-21 12:13:49', NULL),
(4, 'redminote7front-379x800-15598073112215d5d35ac64369.jpeg', 'uploads/products/redminote7front-379x800-15598073112215d5d35ac64369.jpeg', 'uploads/thumbnails/redminote7front-379x800-15598073112215d5d35ac64369.jpeg', 'image/webp', '24338', '199 X 420', 'Image', NULL, NULL, NULL, 'Products', 3, 1, 1, '2019-08-21 12:14:36', '2019-08-21 12:14:36', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `media_settings`
--

DROP TABLE IF EXISTS `media_settings`;
CREATE TABLE IF NOT EXISTS `media_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_id` int(11) NOT NULL,
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `media_settings`
--

INSERT INTO `media_settings` (`id`, `type_id`, `width`, `height`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 25, 25, 1, 1, '2019-08-12 08:35:13', '2019-08-12 08:35:13', NULL),
(2, 2, 100, 100, 1, 1, '2019-08-12 08:35:14', '2019-08-12 08:42:11', '2019-08-12 08:42:11'),
(3, 2, 100, 100, 1, 1, '2019-08-12 08:42:49', '2019-08-12 08:42:49', NULL),
(4, 1, 100, 250, 1, 1, '2019-08-12 08:42:49', '2019-08-12 08:42:49', NULL),
(5, 3, 20, 20, 1, 1, '2019-08-12 08:59:24', '2019-08-12 08:59:24', NULL),
(6, 1, 128, 128, 1, 1, '2019-08-12 08:42:49', '2019-08-12 08:42:49', NULL),
(7, 3, 200, 200, 1, 1, '2019-08-12 08:59:24', '2019-08-12 08:59:24', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `media_types`
--

DROP TABLE IF EXISTS `media_types`;
CREATE TABLE IF NOT EXISTS `media_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(20) NOT NULL,
  `path` varchar(250) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `media_types`
--

INSERT INTO `media_types` (`id`, `type`, `path`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Products', 'public/uploads/products', '2019-08-12 07:31:33', '2019-08-12 07:31:33', NULL),
(2, 'Pages', 'public/uploads/pages', '2019-08-12 07:31:33', '2019-08-12 07:31:33', NULL),
(3, 'Thumbnails', 'public/uploads/thumbnails', '2019-08-12 07:31:33', '2019-08-12 07:31:33', NULL),
(4, 'Users', 'public/uploads/users', '2019-08-12 07:31:33', '2019-08-12 07:31:33', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `product_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tagline` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `vendor_id` int(11) DEFAULT NULL,
  `summary` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `top_description` text COLLATE utf8mb4_unicode_ci,
  `bottom_description` text COLLATE utf8mb4_unicode_ci,
  `quantity` int(11) DEFAULT NULL,
  `mrp` double(10,2) DEFAULT NULL,
  `sale_price` double(10,2) DEFAULT NULL,
  `default_variant_id` int(11) DEFAULT NULL,
  `is_featured_in_home_page` tinyint(1) NOT NULL DEFAULT '0',
  `is_featured_in_category` tinyint(1) NOT NULL DEFAULT '0',
  `is_new` tinyint(1) NOT NULL DEFAULT '0',
  `is_top_seller` tinyint(1) NOT NULL DEFAULT '0',
  `is_today_deal` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `default_image_id` int(11) DEFAULT NULL,
  `page_heading` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `browser_title` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `category_id`, `product_name`, `slug`, `tagline`, `brand_id`, `vendor_id`, `summary`, `top_description`, `bottom_description`, `quantity`, `mrp`, `sale_price`, `default_variant_id`, `is_featured_in_home_page`, `is_featured_in_category`, `is_new`, `is_top_seller`, `is_today_deal`, `is_active`, `default_image_id`, `page_heading`, `browser_title`, `meta_keywords`, `meta_description`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 8, 'Test Product', 'test-product', 'Test Product', NULL, NULL, '<p>Test Product<br></p>', '<p>Test Product<br></p>', '<p>Test Product<br></p>', 100, 10000.00, 9500.00, NULL, 1, 1, 1, 1, 1, 0, NULL, 'Test Product', 'Test Product', 'Test Product', 'Test Product', 1, 1, '2019-08-16 01:56:10', '2019-08-16 01:56:10', NULL),
(2, 2, 'Test product with Varient', 'test-product-with-varient', 'Test product with Varient', NULL, NULL, '<p>Test product with Varient<br></p>', '<p>Test product with Varient<br></p>', '<p>Test product with Varient<br></p>', NULL, NULL, NULL, NULL, 1, 1, 1, 1, 1, 1, NULL, 'Test product with Varient', 'Test product with Varient', 'Test product with Varient', 'Test product with Varient', 1, 1, '2019-08-16 07:12:40', '2019-08-16 07:12:40', NULL),
(3, 2, 'OPPO A5s', 'oppo-a5s', 'OPPO A5s', NULL, NULL, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod \r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim \r\nveniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea \r\ncommodo consequat. Duis aute irure dolor in reprehenderit in voluptate \r\nvelit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint \r\noccaecat cupidatat non proident, sunt in culpa qui officia deserunt \r\nmollit anim id est laborum.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod \r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim \r\nveniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea \r\ncommodo consequat. Duis aute irure dolor in reprehenderit in voluptate \r\nvelit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint \r\noccaecat cupidatat non proident, sunt in culpa qui officia deserunt \r\nmollit anim id est laborum.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod \r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim \r\nveniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea \r\ncommodo consequat. Duis aute irure dolor in reprehenderit in voluptate \r\nvelit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint \r\noccaecat cupidatat non proident, sunt in culpa qui officia deserunt \r\nmollit anim id est laborum.</p>', NULL, NULL, NULL, NULL, 1, 1, 1, 1, 1, 1, NULL, 'OPPO A5s', 'OPPO A5s', 'OPPO A5s', 'OPPO A5s', 1, 1, '2019-08-21 04:22:53', '2019-08-21 04:22:53', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_cateory_attributes`
--

DROP TABLE IF EXISTS `product_cateory_attributes`;
CREATE TABLE IF NOT EXISTS `product_cateory_attributes` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `attribute_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attribute_slug` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `show_as_variant` tinyint(2) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_cateory_attributes`
--

INSERT INTO `product_cateory_attributes` (`id`, `category_id`, `attribute_name`, `attribute_slug`, `show_as_variant`, `created_at`, `updated_at`) VALUES
(6, 2, 'RAM', 'ram', 2, '2019-08-20 05:46:21', '2019-08-20 05:52:11'),
(2, 14, 'Capacity', 'capacity-1', 1, '2019-08-13 04:27:50', '2019-08-13 04:40:17'),
(3, 16, 'Quality', 'quality', 0, '2019-08-13 04:41:03', '2019-08-13 04:41:03'),
(5, 2, 'Colour', 'colour', 1, '2019-08-20 05:45:19', '2019-08-20 05:45:19'),
(7, 2, 'Storage', 'storage', 3, '2019-08-20 05:46:50', '2019-08-20 05:52:30');

-- --------------------------------------------------------

--
-- Table structure for table `product_cateory_attribute_values`
--

DROP TABLE IF EXISTS `product_cateory_attribute_values`;
CREATE TABLE IF NOT EXISTS `product_cateory_attribute_values` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `attribute_id` int(11) NOT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value_slug` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_cateory_attribute_values`
--

INSERT INTO `product_cateory_attribute_values` (`id`, `attribute_id`, `value`, `value_slug`, `created_at`, `updated_at`) VALUES
(1, 3, '30 MB', '30-mb', '2019-08-13 06:57:39', '2019-08-13 06:57:39'),
(3, 3, '50 MB', '50-mb', '2019-08-13 07:21:02', '2019-08-13 07:21:02'),
(4, 2, 'Test', 'test', '2019-08-13 07:23:12', '2019-08-13 07:23:12'),
(5, 2, 'Test2', 'test-2', '2019-08-13 07:23:26', '2019-08-13 07:23:26'),
(6, 2, 'Test3', 'test-3', '2019-08-13 07:23:52', '2019-08-13 07:23:52'),
(7, 5, 'Black', 'black', '2019-08-20 05:53:44', '2019-08-20 05:53:44'),
(8, 5, 'Blue', 'blue', '2019-08-20 05:53:58', '2019-08-20 05:53:58'),
(9, 5, 'Red', 'red', '2019-08-20 05:54:10', '2019-08-20 05:54:10'),
(10, 6, '4 GB', '4gb', '2019-08-20 05:54:38', '2019-08-20 05:54:38'),
(11, 6, '6 GB', '6gb', '2019-08-20 05:54:55', '2019-08-20 05:54:55'),
(12, 7, '128', '128', '2019-08-20 05:55:39', '2019-08-20 05:55:39'),
(13, 7, '256', '256', '2019-08-20 05:55:53', '2019-08-20 05:55:53');

-- --------------------------------------------------------

--
-- Table structure for table `product_variants`
--

DROP TABLE IF EXISTS `product_variants`;
CREATE TABLE IF NOT EXISTS `product_variants` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `products_id` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level1_attribute_value_id` int(11) DEFAULT NULL,
  `level2_attribute_value_id` int(11) DEFAULT NULL,
  `level3_attribute_value_id` int(11) DEFAULT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  `sku` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `retail_price` double(10,2) DEFAULT NULL,
  `sale_price` double(10,2) DEFAULT NULL,
  `landing_price` double(10,2) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `short_description` text COLLATE utf8mb4_unicode_ci,
  `image_id` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_variants`
--

INSERT INTO `product_variants` (`id`, `products_id`, `name`, `slug`, `level1_attribute_value_id`, `level2_attribute_value_id`, `level3_attribute_value_id`, `is_default`, `sku`, `retail_price`, `sale_price`, `landing_price`, `quantity`, `short_description`, `image_id`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 'Test product with Varient', 'test-product-with-varient', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2019-08-16 07:12:41', '2019-08-21 04:16:02', NULL),
(2, 2, 'Test product with Varient (Colour - Black, RAM - 4 GB, Storage - 128)', 'test-product-with-varient-new', 7, 10, 12, 0, 'SKU98989H', 15000.00, 15000.00, 15000.00, NULL, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod \r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim \r\nveniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea \r\ncommodo consequat. Duis aute irure dolor in reprehenderit in voluptate \r\nvelit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint \r\noccaecat cupidatat non proident, sunt in culpa qui officia deserunt \r\nmollit anim id est laborum.</p>', NULL, 1, 1, '2019-08-21 04:10:04', '2019-08-21 04:16:02', NULL),
(3, 2, 'Test product with Varient (Colour - Blue, RAM - 6 GB, Storage - 256)', 'test-product-with-varient-new-blue', 8, 11, 13, 0, 'SKU98989H', 15000.00, 15000.00, 15000.00, NULL, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod \r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim \r\nveniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea \r\ncommodo consequat. Duis aute irure dolor in reprehenderit in voluptate \r\nvelit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint \r\noccaecat cupidatat non proident, sunt in culpa qui officia deserunt \r\nmollit anim id est laborum.</p>', 1, 1, 1, '2019-08-21 04:12:29', '2019-08-21 04:16:02', NULL),
(4, 2, 'Test product with Varient (Colour - Red, RAM - 4 GB, Storage - 128)', 'test-product-with-varient-new-red', 9, 10, 12, 0, 'SKU98989H', 15000.00, 15000.00, 15000.00, NULL, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod \r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim \r\nveniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea \r\ncommodo consequat. Duis aute irure dolor in reprehenderit in voluptate \r\nvelit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint \r\noccaecat cupidatat non proident, sunt in culpa qui officia deserunt \r\nmollit anim id est laborum.</p>', 3, 1, 1, '2019-08-21 04:14:23', '2019-08-21 04:16:02', NULL),
(5, 2, 'Test product with Varient (Colour - Red, RAM - 6 GB, Storage - 256)', 'test-product-with-varient-new-red-1', 9, 11, 13, 0, 'SKU98989H', 15000.00, 15000.00, 15000.00, NULL, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod \r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim \r\nveniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea \r\ncommodo consequat. Duis aute irure dolor in reprehenderit in voluptate \r\nvelit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint \r\noccaecat cupidatat non proident, sunt in culpa qui officia deserunt \r\nmollit anim id est laborum.</p>', 4, 1, 1, '2019-08-21 04:16:02', '2019-08-21 04:16:02', NULL),
(6, 3, 'OPPO A5s', 'oppo-a5s', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2019-08-21 04:22:53', '2019-08-21 04:44:51', NULL),
(7, 3, 'OPPO A5s (Colour - Red, RAM - 4 GB, Storage - 128)', 'oppo-a5s-red', 9, 10, 12, 0, 'SKU98989R', 15000.00, 15000.00, 15000.00, NULL, NULL, 4, 1, 1, '2019-08-21 04:44:51', '2019-08-21 04:44:51', NULL),
(8, 3, 'OPPO A5s (Colour - Black, RAM - 4 GB, Storage - 256)', 'oppo-a5s-black', 7, 10, 13, 0, 'SKU98989B', 15000.00, 15000.00, 15000.00, NULL, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod \r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim \r\nveniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea \r\ncommodo consequat. Duis aute irure dolor in reprehenderit in voluptate \r\nvelit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint \r\noccaecat cupidatat non proident, sunt in culpa qui officia deserunt \r\nmollit anim id est laborum.</p>', 4, 1, 1, '2019-08-21 06:46:41', '2019-08-21 06:46:41', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_variant_images`
--

DROP TABLE IF EXISTS `product_variant_images`;
CREATE TABLE IF NOT EXISTS `product_variant_images` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `variant_id` int(11) NOT NULL,
  `image_id` int(11) NOT NULL,
  `title` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alt` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_common` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_variant_images`
--

INSERT INTO `product_variant_images` (`id`, `variant_id`, `image_id`, `title`, `alt`, `is_common`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 8, 3, 'Image1', 'Image Alt1', 0, '2019-08-21 06:46:41', '2019-08-21 06:46:41', NULL),
(2, 8, 2, 'Image2', 'Image Alt2', 0, '2019-08-21 06:46:41', '2019-08-21 06:46:41', NULL),
(3, 8, 1, 'Image3', 'Image Alt3', 0, '2019-08-21 06:46:41', '2019-08-21 06:46:41', NULL),
(4, 8, 4, 'Image4', 'Image Alt4', 0, '2019-08-21 06:46:41', '2019-08-21 06:46:41', NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
