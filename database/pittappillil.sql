-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Aug 13, 2019 at 05:13 AM
-- Server version: 5.7.26
-- PHP Version: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pittappillil`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_pages`
--

DROP TABLE IF EXISTS `admin_pages`;
CREATE TABLE IF NOT EXISTS `admin_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `slug` varchar(250) NOT NULL,
  `permission` varchar(250) NOT NULL,
  `target` varchar(10) DEFAULT NULL,
  `icon` varchar(50) NOT NULL,
  `parent` int(11) NOT NULL DEFAULT '0',
  `created_by` bigint(20) NOT NULL,
  `updated_by` bigint(20) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_pages`
--

INSERT INTO `admin_pages` (`id`, `title`, `slug`, `permission`, `target`, `icon`, `parent`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Dashboard', 'admin/dashboard', 'dashboard', NULL, 'fa fa-dashboard', 0, 1, 1, '2019-08-02 11:27:10', '2019-08-02 11:27:10'),
(2, 'User', '#', 'user-management', NULL, 'fa fa-user', 0, 1, 1, '2019-08-02 11:27:10', '2019-08-02 11:27:10'),
(3, 'Add User', 'admin/users/create', 'user-add', NULL, 'fa fa-user', 2, 1, 1, '2019-08-02 11:27:10', '2019-08-02 11:27:10'),
(4, 'Manage User', 'admin/users', 'user-management', NULL, 'fa fa-user', 2, 1, 1, '2019-08-02 11:27:10', '2019-08-02 11:27:10'),
(5, 'Settings', '#', 'settings', NULL, 'fa fa-dashboard', 0, 1, 1, '2019-08-02 11:27:10', '2019-08-02 11:27:10'),
(6, 'General Settings', 'admin/settings', 'settings', NULL, 'fa fa-dashboard', 5, 1, 1, '2019-08-02 11:27:10', '2019-08-02 11:27:10'),
(7, 'Image Settings', 'admin/image-settings', 'image-settings', NULL, 'fa fa-dashboard', 5, 1, 1, '2019-08-02 11:27:10', '2019-08-02 11:27:10'),
(8, 'Menu Settings', 'admin/menu', 'menu-settings', NULL, 'fa fa-dashboard', 5, 1, 1, '2019-08-02 11:27:10', '2019-08-02 11:27:10');

-- --------------------------------------------------------

--
-- Table structure for table `bans`
--

DROP TABLE IF EXISTS `bans`;
CREATE TABLE IF NOT EXISTS `bans` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `bannable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bannable_id` bigint(20) UNSIGNED NOT NULL,
  `created_by_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by_id` bigint(20) UNSIGNED DEFAULT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci,
  `expired_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bans_bannable_type_bannable_id_index` (`bannable_type`,`bannable_id`),
  KEY `bans_created_by_type_created_by_id_index` (`created_by_type`,`created_by_id`),
  KEY `bans_expired_at_index` (`expired_at`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `branches`
--

DROP TABLE IF EXISTS `branches`;
CREATE TABLE IF NOT EXISTS `branches` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `branch_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `browser_title` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lattitude` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_code` bigint(11) DEFAULT NULL,
  `phone` bigint(11) DEFAULT NULL,
  `meta_description` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `page_heading` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

DROP TABLE IF EXISTS `brands`;
CREATE TABLE IF NOT EXISTS `brands` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `brand_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `browser_title` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` varchar(520) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `page_heading` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_category_id` int(11) DEFAULT NULL,
  `category_name` varchar(200) NOT NULL,
  `slug` varchar(200) NOT NULL,
  `top_description` longtext,
  `bottom_description` longtext,
  `page_title` longtext,
  `browser_title` varchar(500) DEFAULT NULL,
  `meta_keywords` varchar(500) DEFAULT NULL,
  `meta_description` text,
  `tagline` text,
  `banner_image` int(11) DEFAULT NULL,
  `thumbnail_image` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `parent_category_id`, `category_name`, `slug`, `top_description`, `bottom_description`, `page_title`, `browser_title`, `meta_keywords`, `meta_description`, `tagline`, `banner_image`, `thumbnail_image`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, NULL, 'Ear Phones', 'ear-phones', NULL, NULL, 'Ear Phones', 'Ear Phones', 'Ear Phones', 'Ear Phones', 'Ear Phones', 10, 11, '2019-05-03 01:42:45', '2019-05-03 01:42:45', NULL),
(2, NULL, 'Mobiles', 'mobiles', NULL, NULL, 'Mobiles', 'Mobiles', 'Mobiles', 'Mobiles', 'mobiles', 34, 13, '2019-05-03 01:46:44', '2019-05-07 22:38:14', NULL),
(3, 3, 'TV & Home Theatre', 'tv-home-theatre', '<section><div class=\"row\">\r\n        <div class=\"col-sm-12\" data-type=\"container-content\">\r\n        <section data-type=\"component-text\"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro labore architecto fuga tempore omnis aliquid, rerum numquam deleniti ipsam earum velit aliquam deserunt, molestiae officiis mollitia accusantium suscipit fugiat esse magnam eaque cumque, iste corrupti magni? Illo dicta saepe, maiores fugit aliquid consequuntur aut, rem ex iusto dolorem molestias obcaecati eveniet vel voluptatibus recusandae illum, voluptatem! Odit est possimus nesciunt.</p>\r\n</section></div>\r\n    </div></section>', NULL, 'TV & Home Theatre', 'TV & Home Theatre', 'TV & Home Theatre', 'TV & Home Theatre', 'TV & Home Theatre', 37, 38, '2019-05-08 00:24:11', '2019-08-02 04:42:22', NULL),
(4, 3, 'LED TVs', 'led-tvs', '<section><div class=\"row\">\r\n        <div class=\"col-sm-12\" data-type=\"container-content\">\r\n        <section data-type=\"component-text\"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro labore architecto fuga tempore omnis aliquid, rerum numquam deleniti ipsam earum velit aliquam deserunt, molestiae officiis mollitia accusantium suscipit fugiat esse magnam eaque cumque, iste corrupti magni? Illo dicta saepe, maiores fugit aliquid consequuntur aut, rem ex iusto dolorem molestias obcaecati eveniet vel voluptatibus recusandae illum, voluptatem! Odit est possimus nesciunt.</p>\r\n</section></div>\r\n    </div></section>', NULL, 'LED TVs', 'LED TVs', 'LED TVs', 'LED TVs', 'LED TVs', 39, 40, '2019-05-08 00:26:42', '2019-05-08 00:26:42', NULL),
(5, 3, 'Home Theater', 'home-theater', '<section><div class=\"row\">\r\n        <div class=\"col-sm-12\" data-type=\"container-content\">\r\n        <section data-type=\"component-text\"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro labore architecto fuga tempore omnis aliquid, rerum numquam deleniti ipsam earum velit aliquam deserunt, molestiae officiis mollitia accusantium suscipit fugiat esse magnam eaque cumque, iste corrupti magni? Illo dicta saepe, maiores fugit aliquid consequuntur aut, rem ex iusto dolorem molestias obcaecati eveniet vel voluptatibus recusandae illum, voluptatem! Odit est possimus nesciunt.</p>\r\n</section></div>\r\n    </div></section>', NULL, 'Home Theater', 'Home Theater', 'Home Theater', 'Home Theater', 'Home Theater', 47, 48, '2019-05-08 00:33:43', '2019-05-08 00:33:43', NULL),
(6, 3, 'DVD Players', 'dvd-players', '<section><div class=\"row\">\r\n        <div class=\"col-sm-12\" data-type=\"container-content\">\r\n        <section data-type=\"component-text\"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro labore architecto fuga tempore omnis aliquid, rerum numquam deleniti ipsam earum velit aliquam deserunt, molestiae officiis mollitia accusantium suscipit fugiat esse magnam eaque cumque, iste corrupti magni? Illo dicta saepe, maiores fugit aliquid consequuntur aut, rem ex iusto dolorem molestias obcaecati eveniet vel voluptatibus recusandae illum, voluptatem! Odit est possimus nesciunt.</p>\r\n</section></div>\r\n    </div></section>', NULL, 'DVD Players', 'DVD Players', 'DVD Players', 'DVD Players', 'DVD Players', 51, 52, '2019-05-08 00:36:33', '2019-05-08 00:36:33', NULL),
(7, NULL, 'Major Appliances', 'major-appliances', '<section><div class=\"row\">\r\n        <div class=\"col-sm-12\" data-type=\"container-content\">\r\n        <section data-type=\"component-text\"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro labore architecto fuga tempore omnis aliquid, rerum numquam deleniti ipsam earum velit aliquam deserunt, molestiae officiis mollitia accusantium suscipit fugiat esse magnam eaque cumque, iste corrupti magni? Illo dicta saepe, maiores fugit aliquid consequuntur aut, rem ex iusto dolorem molestias obcaecati eveniet vel voluptatibus recusandae illum, voluptatem! Odit est possimus nesciunt.</p>\r\n</section></div>\r\n    </div></section>', NULL, 'Major Appliances', 'Major Appliances', 'Major Appliances', 'Major Appliances', 'Major Appliances', 55, 56, '2019-05-08 00:38:38', '2019-05-08 00:38:38', NULL),
(8, 7, 'Air Conditioners', 'air-conditioners', '<section><div class=\"row\">\r\n        <div class=\"col-sm-12\" data-type=\"container-content\">\r\n        <section data-type=\"component-text\"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro labore architecto fuga tempore omnis aliquid, rerum numquam deleniti ipsam earum velit aliquam deserunt, molestiae officiis mollitia accusantium suscipit fugiat esse magnam eaque cumque, iste corrupti magni? Illo dicta saepe, maiores fugit aliquid consequuntur aut, rem ex iusto dolorem molestias obcaecati eveniet vel voluptatibus recusandae illum, voluptatem! Odit est possimus nesciunt.</p>\r\n</section></div>\r\n    </div></section>', NULL, 'Air Conditioners', 'Air Conditioners', 'Air Conditioners', 'Air Conditioners', 'Air Conditioners', 57, 58, '2019-05-08 00:39:38', '2019-05-08 00:39:38', NULL),
(9, 7, 'Chimneys & HOODs', 'chimneys-hoods', '<section><div class=\"row\">\r\n        <div class=\"col-sm-12\" data-type=\"container-content\">\r\n        <section data-type=\"component-text\"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro labore architecto fuga tempore omnis aliquid, rerum numquam deleniti ipsam earum velit aliquam deserunt, molestiae officiis mollitia accusantium suscipit fugiat esse magnam eaque cumque, iste corrupti magni? Illo dicta saepe, maiores fugit aliquid consequuntur aut, rem ex iusto dolorem molestias obcaecati eveniet vel voluptatibus recusandae illum, voluptatem! Odit est possimus nesciunt.</p>\r\n</section></div>\r\n    </div></section>', NULL, 'Chimneys & HOODs', 'Chimneys & HOODs', 'Chimneys & HOODs', 'Chimneys & HOODs', 'Chimneys & HOODs', 61, 62, '2019-05-08 00:41:47', '2019-05-08 00:41:47', NULL),
(10, 7, 'Microwave Ovens', 'microwave-ovens', '<section><div class=\"row\">\r\n        <div class=\"col-sm-12\" data-type=\"container-content\">\r\n        <section data-type=\"component-text\"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro labore architecto fuga tempore omnis aliquid, rerum numquam deleniti ipsam earum velit aliquam deserunt, molestiae officiis mollitia accusantium suscipit fugiat esse magnam eaque cumque, iste corrupti magni? Illo dicta saepe, maiores fugit aliquid consequuntur aut, rem ex iusto dolorem molestias obcaecati eveniet vel voluptatibus recusandae illum, voluptatem! Odit est possimus nesciunt.</p>\r\n</section></div>\r\n    </div></section>', NULL, 'Microwave Ovens', 'Microwave Ovens', 'Microwave Ovens', 'Microwave Ovens', 'Microwave Ovens', 65, 66, '2019-05-08 00:44:40', '2019-05-08 00:44:40', NULL),
(11, NULL, 'Small Kitchen Appliances', 'small-kitchen-appliances', '<section><div class=\"row\">\r\n        <div class=\"col-sm-12\" data-type=\"container-content\">\r\n        <section data-type=\"component-text\"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro labore architecto fuga tempore omnis aliquid, rerum numquam deleniti ipsam earum velit aliquam deserunt, molestiae officiis mollitia accusantium suscipit fugiat esse magnam eaque cumque, iste corrupti magni? Illo dicta saepe, maiores fugit aliquid consequuntur aut, rem ex iusto dolorem molestias obcaecati eveniet vel voluptatibus recusandae illum, voluptatem! Odit est possimus nesciunt.</p>\r\n</section></div>\r\n    </div></section>', NULL, 'Small Kitchen Appliances', 'Small Kitchen Appliances', 'Small Kitchen Appliances', 'Small Kitchen Appliances', 'Small Kitchen Appliances', 69, 70, '2019-05-08 00:47:02', '2019-05-08 00:47:02', NULL),
(12, 11, 'Blenders', 'blenders', '<section><div class=\"row\">\r\n        <div class=\"col-sm-12\" data-type=\"container-content\">\r\n        <section data-type=\"component-text\"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro labore architecto fuga tempore omnis aliquid, rerum numquam deleniti ipsam earum velit aliquam deserunt, molestiae officiis mollitia accusantium suscipit fugiat esse magnam eaque cumque, iste corrupti magni? Illo dicta saepe, maiores fugit aliquid consequuntur aut, rem ex iusto dolorem molestias obcaecati eveniet vel voluptatibus recusandae illum, voluptatem! Odit est possimus nesciunt.</p>\r\n</section></div>\r\n    </div></section>', NULL, 'Blenders', 'Blenders', 'Blenders', 'Blenders', 'Blenders', 71, 72, '2019-05-08 00:48:04', '2019-05-08 00:48:04', NULL),
(13, NULL, 'Home & Kitchen Needs', 'home-kitchen-needs', '<section><div class=\"row\">\r\n        <div class=\"col-sm-12\" data-type=\"container-content\">\r\n        <section data-type=\"component-text\"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro labore architecto fuga tempore omnis aliquid, rerum numquam deleniti ipsam earum velit aliquam deserunt, molestiae officiis mollitia accusantium suscipit fugiat esse magnam eaque cumque, iste corrupti magni? Illo dicta saepe, maiores fugit aliquid consequuntur aut, rem ex iusto dolorem molestias obcaecati eveniet vel voluptatibus recusandae illum, voluptatem! Odit est possimus nesciunt.</p>\r\n</section></div>\r\n    </div></section>', NULL, 'Home & Kitchen Needs', 'Home & Kitchen Needs', 'Home & Kitchen Needs', 'Home & Kitchen Needs', 'Home & Kitchen Needs', 75, 76, '2019-05-08 00:50:40', '2019-05-08 00:50:40', NULL),
(14, 13, 'Cookware', 'cookware', '<section><div class=\"row\">\r\n        <div class=\"col-sm-12\" data-type=\"container-content\">\r\n        <section data-type=\"component-text\"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro labore architecto fuga tempore omnis aliquid, rerum numquam deleniti ipsam earum velit aliquam deserunt, molestiae officiis mollitia accusantium suscipit fugiat esse magnam eaque cumque, iste corrupti magni? Illo dicta saepe, maiores fugit aliquid consequuntur aut, rem ex iusto dolorem molestias obcaecati eveniet vel voluptatibus recusandae illum, voluptatem! Odit est possimus nesciunt.</p>\r\n</section></div>\r\n    </div></section>', NULL, 'Cookware', 'Cookware', 'Cookware', 'Cookware', 'Cookware', 77, 78, '2019-05-08 00:51:48', '2019-05-08 00:51:48', NULL),
(15, NULL, 'Audio', 'audio', '<section><div class=\"row\">\r\n        <div class=\"col-sm-12\" data-type=\"container-content\">\r\n        <section data-type=\"component-text\"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro labore architecto fuga tempore omnis aliquid, rerum numquam deleniti ipsam earum velit aliquam deserunt, molestiae officiis mollitia accusantium suscipit fugiat esse magnam eaque cumque, iste corrupti magni? Illo dicta saepe, maiores fugit aliquid consequuntur aut, rem ex iusto dolorem molestias obcaecati eveniet vel voluptatibus recusandae illum, voluptatem! Odit est possimus nesciunt.</p>\r\n</section></div>\r\n    </div></section>', NULL, 'Audio', 'Audio', 'Audio', 'Audio', 'Audio', 83, 84, '2019-05-08 00:56:53', '2019-05-08 00:56:53', NULL),
(16, 15, 'Headphones', 'headphones', '<section><div class=\"row\">\r\n        <div class=\"col-sm-12\" data-type=\"container-content\">\r\n        <section data-type=\"component-text\"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro labore architecto fuga tempore omnis aliquid, rerum numquam deleniti ipsam earum velit aliquam deserunt, molestiae officiis mollitia accusantium suscipit fugiat esse magnam eaque cumque, iste corrupti magni? Illo dicta saepe, maiores fugit aliquid consequuntur aut, rem ex iusto dolorem molestias obcaecati eveniet vel voluptatibus recusandae illum, voluptatem! Odit est possimus nesciunt.</p>\r\n</section></div>\r\n    </div></section>', NULL, 'Headphones', 'Headphones', 'Headphones', 'Headphones', 'Headphones', 85, 86, '2019-05-08 00:57:54', '2019-05-08 00:57:54', NULL),
(17, NULL, 'Personal Care & Beauty', 'personal-care-beauty', '<section><div class=\"row\">\r\n        <div class=\"col-sm-12\" data-type=\"container-content\">\r\n        <section data-type=\"component-text\"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro labore architecto fuga tempore omnis aliquid, rerum numquam deleniti ipsam earum velit aliquam deserunt, molestiae officiis mollitia accusantium suscipit fugiat esse magnam eaque cumque, iste corrupti magni? Illo dicta saepe, maiores fugit aliquid consequuntur aut, rem ex iusto dolorem molestias obcaecati eveniet vel voluptatibus recusandae illum, voluptatem! Odit est possimus nesciunt.</p>\r\n</section></div>\r\n    </div></section>', NULL, 'Personal Care & Beauty', 'Personal Care & Beauty', 'Personal Care & Beauty', 'Personal Care & Beauty', 'Personal Care & Beauty', 91, 92, '2019-05-08 01:02:35', '2019-05-08 01:02:35', NULL),
(18, 17, 'Hair Care', 'hair-care', '<section><div class=\"row\">\r\n        <div class=\"col-sm-12\" data-type=\"container-content\">\r\n        <section data-type=\"component-text\"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro labore architecto fuga tempore omnis aliquid, rerum numquam deleniti ipsam earum velit aliquam deserunt, molestiae officiis mollitia accusantium suscipit fugiat esse magnam eaque cumque, iste corrupti magni? Illo dicta saepe, maiores fugit aliquid consequuntur aut, rem ex iusto dolorem molestias obcaecati eveniet vel voluptatibus recusandae illum, voluptatem! Odit est possimus nesciunt.</p>\r\n</section></div>\r\n    </div></section>', NULL, 'Hair Care', 'Hair Care', 'Hair Care', 'Hair Care', 'Hair Care', 93, 94, '2019-05-08 01:04:03', '2019-05-08 01:04:03', NULL),
(19, NULL, 'Amazon Products', 'amazon-products', '<section><div class=\"row\">\r\n        <div class=\"col-sm-12\" data-type=\"container-content\">\r\n        <section data-type=\"component-text\"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro labore architecto fuga tempore omnis aliquid, rerum numquam deleniti ipsam earum velit aliquam deserunt, molestiae officiis mollitia accusantium suscipit fugiat esse magnam eaque cumque, iste corrupti magni? Illo dicta saepe, maiores fugit aliquid consequuntur aut, rem ex iusto dolorem molestias obcaecati eveniet vel voluptatibus recusandae illum, voluptatem! Odit est possimus nesciunt.</p>\r\n</section></div>\r\n    </div></section>', NULL, 'Amazon Products', 'Amazon Products', 'Amazon Products', 'Amazon Products', 'Amazon Products', 99, 100, '2019-05-08 17:48:24', '2019-05-08 17:48:24', NULL),
(20, 19, 'Echo', 'echo', '<section><div class=\"row\">\r\n        <div class=\"col-sm-12\" data-type=\"container-content\">\r\n        <section data-type=\"component-text\"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro labore architecto fuga tempore omnis aliquid, rerum numquam deleniti ipsam earum velit aliquam deserunt, molestiae officiis mollitia accusantium suscipit fugiat esse magnam eaque cumque, iste corrupti magni? Illo dicta saepe, maiores fugit aliquid consequuntur aut, rem ex iusto dolorem molestias obcaecati eveniet vel voluptatibus recusandae illum, voluptatem! Odit est possimus nesciunt.</p>\r\n</section></div>\r\n    </div></section>', NULL, 'Echo', 'Echo', 'Echo', 'Echo', 'Echo', 101, 102, '2019-05-08 17:50:32', '2019-05-08 17:50:32', NULL),
(21, NULL, 'Air Conditioners', 'air-conditionershjhj', NULL, NULL, 'aabbbbbbbbbbbbbb', NULL, NULL, NULL, NULL, 55, 56, '2019-08-01 07:14:33', '2019-08-01 07:14:33', NULL),
(22, 3, 'ir Conditioners', 'ddddddddddddddddddddd', NULL, NULL, 'aabbbbbbbbbbbbbb', NULL, NULL, NULL, 'Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...', NULL, NULL, '2019-08-02 00:15:08', '2019-08-02 00:15:08', NULL),
(23, 4, 'Air Conditioners', 'ddddddddddddddffdsfdsfsdddddddd', NULL, NULL, 'aabbbbbbbbbbbbbb', NULL, NULL, NULL, 'Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...', NULL, NULL, '2019-08-02 00:16:27', '2019-08-02 00:16:27', NULL),
(24, 4, 'Air Conditionersf', 'dfgdgdfdfgdfgg dgere', NULL, NULL, 'aabbbbbbbbbbbbbb', NULL, NULL, NULL, 'dfgdfg', NULL, NULL, '2019-08-02 00:17:04', '2019-08-02 00:17:04', NULL),
(25, 23, 'Air Conditionersf', 'dfgdgdfdfgdfgg dgereghjjgj', NULL, NULL, 'aabbbbbbbbbbbbbb', NULL, NULL, NULL, 'dfgdfg', NULL, NULL, '2019-08-02 00:17:33', '2019-08-02 00:17:33', NULL),
(26, 2, 'Mobiles', 'Mobiles-2', '<p>Mobiles<br></p>', '<p>Mobiles<br></p>', 'Mobiles', 'Mobiles', 'Mobiles', 'Mobiles', 'A Brief About Us', 57, 58, '2019-08-02 01:17:26', '2019-08-02 01:17:26', NULL),
(27, 2, 'Mobiles', 'Mobiles-2-22', '<p>Mobiles<br></p>', '<p>Mobiles<br></p>', 'Mobiles', 'Mobiles', 'Mobiles', 'Mobiles', 'A Brief About Us', NULL, NULL, '2019-08-02 01:44:55', '2019-08-02 01:44:55', NULL),
(28, 4, 'Mobiles 1', 'Mobiles-2-22-2 1', '<p><b>Mobilesn </b><br></p>', '<p><u>Mobiles</u><br></p>', 'Mobiles 1', 'Mobiles 1', 'Mobiles 2', 'Mobiles 3', 'A Brief About Us 1', 64, 63, '2019-08-02 01:45:03', '2019-08-02 02:13:20', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
CREATE TABLE IF NOT EXISTS `countries` (
  `id` int(11) NOT NULL,
  `sortname` varchar(3) NOT NULL,
  `name` varchar(150) NOT NULL,
  `phonecode` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `sortname`, `name`, `phonecode`) VALUES
(1, 'AF', 'Afghanistan', 93),
(2, 'AL', 'Albania', 355),
(3, 'DZ', 'Algeria', 213),
(4, 'AS', 'American Samoa', 1684),
(5, 'AD', 'Andorra', 376),
(6, 'AO', 'Angola', 244),
(7, 'AI', 'Anguilla', 1264),
(8, 'AQ', 'Antarctica', 0),
(9, 'AG', 'Antigua And Barbuda', 1268),
(10, 'AR', 'Argentina', 54),
(11, 'AM', 'Armenia', 374),
(12, 'AW', 'Aruba', 297),
(13, 'AU', 'Australia', 61),
(14, 'AT', 'Austria', 43),
(15, 'AZ', 'Azerbaijan', 994),
(16, 'BS', 'Bahamas The', 1242),
(17, 'BH', 'Bahrain', 973),
(18, 'BD', 'Bangladesh', 880),
(19, 'BB', 'Barbados', 1246),
(20, 'BY', 'Belarus', 375),
(21, 'BE', 'Belgium', 32),
(22, 'BZ', 'Belize', 501),
(23, 'BJ', 'Benin', 229),
(24, 'BM', 'Bermuda', 1441),
(25, 'BT', 'Bhutan', 975),
(26, 'BO', 'Bolivia', 591),
(27, 'BA', 'Bosnia and Herzegovina', 387),
(28, 'BW', 'Botswana', 267),
(29, 'BV', 'Bouvet Island', 0),
(30, 'BR', 'Brazil', 55),
(31, 'IO', 'British Indian Ocean Territory', 246),
(32, 'BN', 'Brunei', 673),
(33, 'BG', 'Bulgaria', 359),
(34, 'BF', 'Burkina Faso', 226),
(35, 'BI', 'Burundi', 257),
(36, 'KH', 'Cambodia', 855),
(37, 'CM', 'Cameroon', 237),
(38, 'CA', 'Canada', 1),
(39, 'CV', 'Cape Verde', 238),
(40, 'KY', 'Cayman Islands', 1345),
(41, 'CF', 'Central African Republic', 236),
(42, 'TD', 'Chad', 235),
(43, 'CL', 'Chile', 56),
(44, 'CN', 'China', 86),
(45, 'CX', 'Christmas Island', 61),
(46, 'CC', 'Cocos (Keeling) Islands', 672),
(47, 'CO', 'Colombia', 57),
(48, 'KM', 'Comoros', 269),
(49, 'CG', 'Republic Of The Congo', 242),
(50, 'CD', 'Democratic Republic Of The Congo', 242),
(51, 'CK', 'Cook Islands', 682),
(52, 'CR', 'Costa Rica', 506),
(53, 'CI', 'Cote D\'Ivoire (Ivory Coast)', 225),
(54, 'HR', 'Croatia (Hrvatska)', 385),
(55, 'CU', 'Cuba', 53),
(56, 'CY', 'Cyprus', 357),
(57, 'CZ', 'Czech Republic', 420),
(58, 'DK', 'Denmark', 45),
(59, 'DJ', 'Djibouti', 253),
(60, 'DM', 'Dominica', 1767),
(61, 'DO', 'Dominican Republic', 1809),
(62, 'TP', 'East Timor', 670),
(63, 'EC', 'Ecuador', 593),
(64, 'EG', 'Egypt', 20),
(65, 'SV', 'El Salvador', 503),
(66, 'GQ', 'Equatorial Guinea', 240),
(67, 'ER', 'Eritrea', 291),
(68, 'EE', 'Estonia', 372),
(69, 'ET', 'Ethiopia', 251),
(70, 'XA', 'External Territories of Australia', 61),
(71, 'FK', 'Falkland Islands', 500),
(72, 'FO', 'Faroe Islands', 298),
(73, 'FJ', 'Fiji Islands', 679),
(74, 'FI', 'Finland', 358),
(75, 'FR', 'France', 33),
(76, 'GF', 'French Guiana', 594),
(77, 'PF', 'French Polynesia', 689),
(78, 'TF', 'French Southern Territories', 0),
(79, 'GA', 'Gabon', 241),
(80, 'GM', 'Gambia The', 220),
(81, 'GE', 'Georgia', 995),
(82, 'DE', 'Germany', 49),
(83, 'GH', 'Ghana', 233),
(84, 'GI', 'Gibraltar', 350),
(85, 'GR', 'Greece', 30),
(86, 'GL', 'Greenland', 299),
(87, 'GD', 'Grenada', 1473),
(88, 'GP', 'Guadeloupe', 590),
(89, 'GU', 'Guam', 1671),
(90, 'GT', 'Guatemala', 502),
(91, 'XU', 'Guernsey and Alderney', 44),
(92, 'GN', 'Guinea', 224),
(93, 'GW', 'Guinea-Bissau', 245),
(94, 'GY', 'Guyana', 592),
(95, 'HT', 'Haiti', 509),
(96, 'HM', 'Heard and McDonald Islands', 0),
(97, 'HN', 'Honduras', 504),
(98, 'HK', 'Hong Kong S.A.R.', 852),
(99, 'HU', 'Hungary', 36),
(100, 'IS', 'Iceland', 354),
(101, 'IN', 'India', 91),
(102, 'ID', 'Indonesia', 62),
(103, 'IR', 'Iran', 98),
(104, 'IQ', 'Iraq', 964),
(105, 'IE', 'Ireland', 353),
(106, 'IL', 'Israel', 972),
(107, 'IT', 'Italy', 39),
(108, 'JM', 'Jamaica', 1876),
(109, 'JP', 'Japan', 81),
(110, 'XJ', 'Jersey', 44),
(111, 'JO', 'Jordan', 962),
(112, 'KZ', 'Kazakhstan', 7),
(113, 'KE', 'Kenya', 254),
(114, 'KI', 'Kiribati', 686),
(115, 'KP', 'Korea North', 850),
(116, 'KR', 'Korea South', 82),
(117, 'KW', 'Kuwait', 965),
(118, 'KG', 'Kyrgyzstan', 996),
(119, 'LA', 'Laos', 856),
(120, 'LV', 'Latvia', 371),
(121, 'LB', 'Lebanon', 961),
(122, 'LS', 'Lesotho', 266),
(123, 'LR', 'Liberia', 231),
(124, 'LY', 'Libya', 218),
(125, 'LI', 'Liechtenstein', 423),
(126, 'LT', 'Lithuania', 370),
(127, 'LU', 'Luxembourg', 352),
(128, 'MO', 'Macau S.A.R.', 853),
(129, 'MK', 'Macedonia', 389),
(130, 'MG', 'Madagascar', 261),
(131, 'MW', 'Malawi', 265),
(132, 'MY', 'Malaysia', 60),
(133, 'MV', 'Maldives', 960),
(134, 'ML', 'Mali', 223),
(135, 'MT', 'Malta', 356),
(136, 'XM', 'Man (Isle of)', 44),
(137, 'MH', 'Marshall Islands', 692),
(138, 'MQ', 'Martinique', 596),
(139, 'MR', 'Mauritania', 222),
(140, 'MU', 'Mauritius', 230),
(141, 'YT', 'Mayotte', 269),
(142, 'MX', 'Mexico', 52),
(143, 'FM', 'Micronesia', 691),
(144, 'MD', 'Moldova', 373),
(145, 'MC', 'Monaco', 377),
(146, 'MN', 'Mongolia', 976),
(147, 'MS', 'Montserrat', 1664),
(148, 'MA', 'Morocco', 212),
(149, 'MZ', 'Mozambique', 258),
(150, 'MM', 'Myanmar', 95),
(151, 'NA', 'Namibia', 264),
(152, 'NR', 'Nauru', 674),
(153, 'NP', 'Nepal', 977),
(154, 'AN', 'Netherlands Antilles', 599),
(155, 'NL', 'Netherlands The', 31),
(156, 'NC', 'New Caledonia', 687),
(157, 'NZ', 'New Zealand', 64),
(158, 'NI', 'Nicaragua', 505),
(159, 'NE', 'Niger', 227),
(160, 'NG', 'Nigeria', 234),
(161, 'NU', 'Niue', 683),
(162, 'NF', 'Norfolk Island', 672),
(163, 'MP', 'Northern Mariana Islands', 1670),
(164, 'NO', 'Norway', 47),
(165, 'OM', 'Oman', 968),
(166, 'PK', 'Pakistan', 92),
(167, 'PW', 'Palau', 680),
(168, 'PS', 'Palestinian Territory Occupied', 970),
(169, 'PA', 'Panama', 507),
(170, 'PG', 'Papua new Guinea', 675),
(171, 'PY', 'Paraguay', 595),
(172, 'PE', 'Peru', 51),
(173, 'PH', 'Philippines', 63),
(174, 'PN', 'Pitcairn Island', 0),
(175, 'PL', 'Poland', 48),
(176, 'PT', 'Portugal', 351),
(177, 'PR', 'Puerto Rico', 1787),
(178, 'QA', 'Qatar', 974),
(179, 'RE', 'Reunion', 262),
(180, 'RO', 'Romania', 40),
(181, 'RU', 'Russia', 70),
(182, 'RW', 'Rwanda', 250),
(183, 'SH', 'Saint Helena', 290),
(184, 'KN', 'Saint Kitts And Nevis', 1869),
(185, 'LC', 'Saint Lucia', 1758),
(186, 'PM', 'Saint Pierre and Miquelon', 508),
(187, 'VC', 'Saint Vincent And The Grenadines', 1784),
(188, 'WS', 'Samoa', 684),
(189, 'SM', 'San Marino', 378),
(190, 'ST', 'Sao Tome and Principe', 239),
(191, 'SA', 'Saudi Arabia', 966),
(192, 'SN', 'Senegal', 221),
(193, 'RS', 'Serbia', 381),
(194, 'SC', 'Seychelles', 248),
(195, 'SL', 'Sierra Leone', 232),
(196, 'SG', 'Singapore', 65),
(197, 'SK', 'Slovakia', 421),
(198, 'SI', 'Slovenia', 386),
(199, 'XG', 'Smaller Territories of the UK', 44),
(200, 'SB', 'Solomon Islands', 677),
(201, 'SO', 'Somalia', 252),
(202, 'ZA', 'South Africa', 27),
(203, 'GS', 'South Georgia', 0),
(204, 'SS', 'South Sudan', 211),
(205, 'ES', 'Spain', 34),
(206, 'LK', 'Sri Lanka', 94),
(207, 'SD', 'Sudan', 249),
(208, 'SR', 'Suriname', 597),
(209, 'SJ', 'Svalbard And Jan Mayen Islands', 47),
(210, 'SZ', 'Swaziland', 268),
(211, 'SE', 'Sweden', 46),
(212, 'CH', 'Switzerland', 41),
(213, 'SY', 'Syria', 963),
(214, 'TW', 'Taiwan', 886),
(215, 'TJ', 'Tajikistan', 992),
(216, 'TZ', 'Tanzania', 255),
(217, 'TH', 'Thailand', 66),
(218, 'TG', 'Togo', 228),
(219, 'TK', 'Tokelau', 690),
(220, 'TO', 'Tonga', 676),
(221, 'TT', 'Trinidad And Tobago', 1868),
(222, 'TN', 'Tunisia', 216),
(223, 'TR', 'Turkey', 90),
(224, 'TM', 'Turkmenistan', 7370),
(225, 'TC', 'Turks And Caicos Islands', 1649),
(226, 'TV', 'Tuvalu', 688),
(227, 'UG', 'Uganda', 256),
(228, 'UA', 'Ukraine', 380),
(229, 'AE', 'United Arab Emirates', 971),
(230, 'GB', 'United Kingdom', 44),
(231, 'US', 'United States', 1),
(232, 'UM', 'United States Minor Outlying Islands', 1),
(233, 'UY', 'Uruguay', 598),
(234, 'UZ', 'Uzbekistan', 998),
(235, 'VU', 'Vanuatu', 678),
(236, 'VA', 'Vatican City State (Holy See)', 39),
(237, 'VE', 'Venezuela', 58),
(238, 'VN', 'Vietnam', 84),
(239, 'VG', 'Virgin Islands (British)', 1284),
(240, 'VI', 'Virgin Islands (US)', 1340),
(241, 'WF', 'Wallis And Futuna Islands', 681),
(242, 'EH', 'Western Sahara', 212),
(243, 'YE', 'Yemen', 967),
(244, 'YU', 'Yugoslavia', 38),
(245, 'ZM', 'Zambia', 260),
(246, 'ZW', 'Zimbabwe', 263);

-- --------------------------------------------------------

--
-- Table structure for table `media_library`
--

DROP TABLE IF EXISTS `media_library`;
CREATE TABLE IF NOT EXISTS `media_library` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_name` text NOT NULL,
  `file_path` varchar(250) NOT NULL,
  `file_type` varchar(100) NOT NULL,
  `file_size` varchar(100) NOT NULL,
  `dimensions` varchar(50) DEFAULT NULL,
  `media_type` varchar(120) NOT NULL DEFAULT 'Image',
  `title` varchar(250) DEFAULT NULL,
  `description` mediumtext,
  `alt_text` varchar(250) DEFAULT NULL,
  `created_by` bigint(20) NOT NULL,
  `updated_by` bigint(20) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `media_library`
--

INSERT INTO `media_library` (`id`, `file_name`, `file_path`, `file_type`, `file_size`, `dimensions`, `media_type`, `title`, `description`, `alt_text`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'eye_logo_5.png', 'uploads/settings/eye_logo_5.png', 'image/png', '9817', '220 X 91', 'Image', NULL, NULL, NULL, 1, 1, '2019-08-10 07:13:11', '2019-08-10 07:13:11', NULL),
(2, 'eye_logo_6.png', 'uploads/settings/eye_logo_6.png', 'image/png', '9817', '220 X 91', 'Image', NULL, NULL, NULL, 1, 1, '2019-08-10 07:14:34', '2019-08-10 07:14:34', NULL),
(3, 'bake-baked-bakery-1028714-min.jpg', 'uploads/settings/bake-baked-bakery-1028714-min.jpg', 'image/jpeg', '424576', '1920 X 1280', 'Image', NULL, NULL, NULL, 1, 1, '2019-08-12 05:27:05', '2019-08-12 05:27:05', NULL),
(4, 'geethu2.jpg', 'uploads/settings/geethu2.jpg', 'image/jpeg', '105975', '750 X 500', 'Image', NULL, NULL, NULL, 1, 1, '2019-08-12 08:59:52', '2019-08-12 08:59:52', NULL),
(5, 'geethu3.jpg', 'uploads/settings/geethu3.jpg', 'image/jpeg', '147443', '800 X 531', 'Image', NULL, NULL, NULL, 1, 1, '2019-08-12 09:02:25', '2019-08-12 09:02:25', NULL),
(6, 'geethu1.jpg', 'uploads/settings/geethu1.jpg', 'image/jpeg', '102202', '830 X 553', 'Image', NULL, NULL, NULL, 1, 1, '2019-08-12 09:04:37', '2019-08-12 09:04:37', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `media_settings`
--

DROP TABLE IF EXISTS `media_settings`;
CREATE TABLE IF NOT EXISTS `media_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_id` int(11) NOT NULL,
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `media_settings`
--

INSERT INTO `media_settings` (`id`, `type_id`, `width`, `height`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 25, 25, 1, 1, '2019-08-12 08:35:13', '2019-08-12 08:35:13', NULL),
(2, 2, 100, 100, 1, 1, '2019-08-12 08:35:14', '2019-08-12 08:42:11', '2019-08-12 08:42:11'),
(3, 2, 100, 100, 1, 1, '2019-08-12 08:42:49', '2019-08-12 08:42:49', NULL),
(4, 1, 100, 250, 1, 1, '2019-08-12 08:42:49', '2019-08-12 08:42:49', NULL),
(5, 3, 20, 20, 1, 1, '2019-08-12 08:59:24', '2019-08-12 08:59:24', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `media_types`
--

DROP TABLE IF EXISTS `media_types`;
CREATE TABLE IF NOT EXISTS `media_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(20) NOT NULL,
  `path` varchar(250) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `media_types`
--

INSERT INTO `media_types` (`id`, `type`, `path`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Products', 'public/uploads/products', '2019-08-12 07:31:33', '2019-08-12 07:31:33', NULL),
(2, 'Pages', 'public/uploads/pages', '2019-08-12 07:31:33', '2019-08-12 07:31:33', NULL),
(3, 'Thumbnails', 'public/uploads/thumbnails', '2019-08-12 07:31:33', '2019-08-12 07:31:33', NULL),
(4, 'Users', 'public/uploads/users', '2019-08-12 07:31:33', '2019-08-12 07:31:33', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

DROP TABLE IF EXISTS `menus`;
CREATE TABLE IF NOT EXISTS `menus` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(2, 'Main Menu', '2019-08-12 06:28:28', '2019-08-12 06:28:28');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

DROP TABLE IF EXISTS `menu_items`;
CREATE TABLE IF NOT EXISTS `menu_items` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `label` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `sort` int(11) NOT NULL DEFAULT '0',
  `class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `menu` int(10) UNSIGNED NOT NULL,
  `depth` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `menu_items_menu_foreign` (`menu`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `label`, `link`, `parent`, `sort`, `class`, `menu`, `depth`, `created_at`, `updated_at`) VALUES
(1, 'Home', 'http://www.google.com', 0, 1, NULL, 2, 0, '2019-08-12 06:28:48', '2019-08-12 06:28:48');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_01_110807_create_permission_tables', 1),
(4, '2017_03_04_000000_create_bans_table', 2),
(5, '2017_08_11_073824_create_menus_wp_table', 2),
(6, '2017_08_11_074006_create_menu_items_wp_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

DROP TABLE IF EXISTS `model_has_permissions`;
CREATE TABLE IF NOT EXISTS `model_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

DROP TABLE IF EXISTS `model_has_roles`;
CREATE TABLE IF NOT EXISTS `model_has_roles` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(250) NOT NULL,
  `title` varchar(250) NOT NULL,
  `description` mediumtext,
  `browser_title` varchar(250) DEFAULT NULL,
  `meta_description` text,
  `meta_keywords` text,
  `media_id` int(11) DEFAULT NULL,
  `video_id` int(11) DEFAULT NULL,
  `youtube_id` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `apply_button_top` tinyint(1) NOT NULL DEFAULT '0',
  `apply_button_bottom` tinyint(1) NOT NULL DEFAULT '0',
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `media_id` (`media_id`),
  KEY `video_id` (`video_id`),
  KEY `youtube_id` (`youtube_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'web', '2019-08-02 00:00:39', '2019-08-02 00:00:39');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

DROP TABLE IF EXISTS `role_has_permissions`;
CREATE TABLE IF NOT EXISTS `role_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `role_has_permissions_role_id_foreign` (`role_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `role_users`
--

DROP TABLE IF EXISTS `role_users`;
CREATE TABLE IF NOT EXISTS `role_users` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `role_id` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role_users`
--

INSERT INTO `role_users` (`user_id`, `role_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2019-08-01 23:55:40', '2019-08-01 23:55:40'),
(2, 1, '2019-08-06 07:22:48', '2019-08-06 07:22:48'),
(3, 1, '2019-08-07 00:03:15', '2019-08-07 00:03:15');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(250) NOT NULL,
  `value` varchar(250) DEFAULT NULL,
  `media_id` int(11) DEFAULT NULL,
  `type` varchar(20) NOT NULL,
  `page` varchar(250) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `code`, `value`, `media_id`, `type`, `page`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'title', 'Pittappillil', NULL, 'Text', NULL, 1, 1, '2019-08-10 07:14:34', '2019-08-12 05:27:30', NULL),
(2, 'logo', 'geethu1.jpg', 6, 'Image', NULL, 1, 1, '2019-08-10 07:14:35', '2019-08-12 09:04:37', NULL),
(3, 'Footer-text1', 'Footer Text1', NULL, 'Text', NULL, 1, 1, '2019-08-12 05:28:30', '2019-08-12 05:28:30', NULL),
(4, 'Footer-text2', 'Footer Text2 Editted', NULL, 'Text', NULL, 1, 1, '2019-08-12 05:28:30', '2019-08-12 05:30:20', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

DROP TABLE IF EXISTS `states`;
CREATE TABLE IF NOT EXISTS `states` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `CountryID` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`id`, `CountryID`, `name`) VALUES
(36, 101, 'ANDHRA PRADESH'),
(37, 101, 'ASSAM'),
(38, 101, 'ARUNACHAL PRADESH'),
(39, 101, 'GUJRAT'),
(40, 101, 'BIHAR'),
(41, 101, 'HARYANA'),
(42, 101, 'HIMACHAL PRADESH'),
(43, 101, 'JAMMU & KASHMIR'),
(44, 101, 'KARNATAKA'),
(45, 101, 'KERALA'),
(46, 101, 'MADHYA PRADESH'),
(47, 101, 'MAHARASHTRA'),
(48, 101, 'MANIPUR'),
(49, 101, 'MEGHALAYA'),
(50, 101, 'MIZORAM'),
(51, 101, 'NAGALAND'),
(52, 101, 'ORISSA'),
(53, 101, 'PUNJAB'),
(54, 101, 'RAJASTHAN'),
(55, 101, 'SIKKIM'),
(56, 101, 'TAMIL NADU'),
(57, 101, 'TRIPURA'),
(58, 101, 'UTTAR PRADESH'),
(59, 101, 'WEST BENGAL'),
(60, 101, 'DELHI'),
(61, 101, 'GOA'),
(62, 101, 'PONDICHERY'),
(63, 101, 'LAKSHDWEEP'),
(64, 101, 'DAMAN & DIU'),
(65, 101, 'DADRA & NAGAR'),
(66, 101, 'CHANDIGARH'),
(67, 101, 'ANDAMAN & NICOBAR'),
(68, 101, 'UTTARANCHAL'),
(69, 101, 'JHARKHAND'),
(70, 101, 'CHATTISGARH');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_code` int(11) DEFAULT NULL,
  `phone_number` bigint(100) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `pin_code` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banned_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `first_name`, `last_name`, `email`, `email_verified_at`, `password`, `remember_token`, `country_code`, `phone_number`, `country_id`, `state_id`, `pin_code`, `address`, `banned_at`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'spider-admin', 'Sobha', 'Sudheesh', 'sobha@spiderworks.in', NULL, '$2y$10$Qum8cKcyuGu/AcZ9rFbBIuFgok0oDTTPCmyNlxGh93eGxqmYckxd2', 'V1Hbtl3BNr20Wz5ieKyP5fc7WKZvv86iyoH944Xnp4vg1s3lW4AjC0Th4jbS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-08-01 06:12:16', '2019-08-07 03:34:42', NULL),
(2, 'test1.last@gmail.com', 'Test1', 'Last', 'test1.last@gmail.com', NULL, '$2y$10$QsNtNcSoFynwlvG6eEXexeCXxCq15jb4xfoG.Gz7YfY8au6V/eyyK', NULL, NULL, NULL, 101, 45, '6878787', 'Test Address', NULL, '2019-08-06 07:22:46', '2019-08-07 08:58:26', NULL),
(3, 'test2.last2@gmail.com', 'Test2', 'Last2', 'test2.last2@gmail.com', NULL, '$2y$10$NnwT4iqX1yFxycdgLlndSeQlerPZ4Mmaq6mJ45KJ62OlinSOtPf/y', NULL, 91, 9898989898, 101, 45, '683517', 'Test2 address editted', NULL, '2019-08-07 00:03:15', '2019-08-07 08:59:36', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vendors`
--

DROP TABLE IF EXISTS `vendors`;
CREATE TABLE IF NOT EXISTS `vendors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vendor_name` varchar(250) NOT NULL,
  `page_heading` varchar(250) NOT NULL,
  `slug` varchar(250) NOT NULL,
  `contact_name` varchar(250) DEFAULT NULL,
  `phone_code` int(11) DEFAULT NULL,
  `phone` bigint(11) DEFAULT NULL,
  `address` text,
  `email` varchar(250) DEFAULT NULL,
  `description` text,
  `browser_title` varchar(250) DEFAULT NULL,
  `meta_description` text,
  `meta_keywords` varchar(250) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
