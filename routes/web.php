<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/config-cache', function() {
    $exitCode = Artisan::call('config:cache');
    return '<h1>Clear Config cleared</h1>';
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/select2/country', 'PluginsController@select2_countries')->name('select2.country');
Route::get('/select2/state/{country_id?}', 'PluginsController@select2_states')->name('select2.state');
Route::get('/select2/categories', 'PluginsController@select2_categories')->name('select2.categories');
Route::get('/select2/brands', 'PluginsController@select2_brands')->name('select2.brands');
Route::get('/select2/venders', 'PluginsController@select2_venders')->name('select2.venders');

Route::get('/validation/user', 'PluginsController@unique_user')->name('validate.unique_user');
Route::get('/validation/attribute-slug', 'PluginsController@unique_attribute_slug')->name('validate.unique_attribute_slug');
Route::get('/validation/attribute-value-slug', 'PluginsController@unique_attribute_value_slug')->name('validate.unique_attribute_value_slug');
Route::get('/validation/product-slug', 'PluginsController@unique_product_slug')->name('validate.unique_product_slug');
Route::get('/validation/product-variant-slug', 'PluginsController@unique_product_variant_slug')->name('validate.unique_product_variant_slug');

Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function(){
	Route::get('/', 'AdminController@login');
    Route::group(['middleware' => ['isAdmin']], function(){
    	Route::get('/dashboard', ['as' => 'admin.dashboard.index', 'uses' => 'AdminController@index' ]);

    	//users
    	Route::get('/users/edit/{id}', 'UserController@edit')->name('admin.users.edit');
    	Route::get('/users/destroy/{id}', 'UserController@destroy')->name('admin.users.destroy');
    	Route::get('/users/create', 'UserController@create')->name('admin.users.create');
    	Route::post('/users/update', 'UserController@update')->name('admin.users.update');
    	Route::get('/users/change-status/{id}', 'UserController@changeStatus')->name('admin.users.change-status');
    	Route::post('/users/store', 'UserController@store')->name('admin.users.store');
    	Route::get('/users', 'UserController@index')->name('admin.users.index');

    	//settings
    	Route::get('/settings/edit/{id}', 'SettingsController@edit')->name('admin.settings.edit');
    	Route::get('/settings/create', 'SettingsController@create')->name('admin.settings.create');
    	Route::post('/settings/update', 'SettingsController@update')->name('admin.settings.update');
    	Route::post('/settings/store', 'SettingsController@store')->name('admin.settings.store');
    	Route::get('/settings', 'SettingsController@index')->name('admin.settings.index');
        Route::get('/settings/destroy/{id}', 'SettingsController@destroy')->name('admin.settings.destroy');

        Route::get('/menu', function(){
            return view('admin/menu');
        })->name('admin.settings.destroy');

    	//image settings
    	Route::get('/image-settings/edit/{id}', 'ImageSettingsController@edit')->name('admin.image-settings.edit');
    	Route::get('/image-settings/create', 'ImageSettingsController@create')->name('admin.image-settings.create');
    	Route::post('/image-settings/update', 'ImageSettingsController@update')->name('admin.image-settings.update');
    	Route::post('/image-settings/store', 'ImageSettingsController@store')->name('admin.image-settings.store');
    	Route::get('/image-settings', 'ImageSettingsController@index')->name('admin.image-settings.index');
        Route::get('/image-settings/destroy/{id}', 'ImageSettingsController@destroy')->name('admin.image-settings.destroy');

        //products
        Route::get('/products/edit/{id}/{tab?}', 'ProductsController@edit')->name('admin.products.edit');
        Route::get('/products/destroy/{id}', 'ProductsController@destroy')->name('admin.products.destroy');
        Route::get('/products/create', 'ProductsController@create')->name('admin.products.create');
        Route::post('/products/update', 'ProductsController@update')->name('admin.products.update');
        Route::get('/products/change-status/{id}', 'ProductsController@changeStatus')->name('admin.products.change-status');
        Route::post('/products/store', 'ProductsController@store')->name('admin.products.store');
        Route::get('/products', 'ProductsController@index')->name('admin.products.index');
        Route::post('/products/media-save', 'ProductsController@mediaSave')->name('admin.products.media-save');

        Route::group(['prefix' => 'products', 'namespace' => 'Products'], function(){
            Route::get('/variants/edit/{id}', 'VariantsController@edit')->name('admin.products.variants.edit');
            Route::get('/variants/destroy/{id}', 'VariantsController@destroy')->name('admin.products.variants.destroy');
            Route::get('/variants/create/{pid}', 'VariantsController@create')->name('admin.products.variants.create');
            Route::post('/variants/update', 'VariantsController@update')->name('admin.products.variants.update');
            Route::get('/variants/change-status/{id}', 'VariantsController@changeStatus')->name('admin.products.variants.change-status');
            Route::post('/variants/store', 'VariantsController@store')->name('admin.products.variants.store');
            Route::get('/variants/{pid}', 'VariantsController@index')->name('admin.products.variants.index');
        });

        //roles
        Route::get('/roles/edit/{id}', 'RolesController@edit')->name('admin.roles.edit');
        Route::get('/roles/destroy/{id}', 'RolesController@destroy')->name('admin.roles.destroy');
        Route::get('/roles/create', 'RolesController@create')->name('admin.roles.create');
        Route::post('/roles/update', 'RolesController@update')->name('admin.roles.update');
        Route::get('/roles/change-status/{id}', 'RolesController@changeStatus')->name('admin.roles.change-status');
        Route::post('/roles/store', 'RolesController@store')->name('admin.roles.store');
        Route::get('/roles', 'RolesController@index')->name('admin.roles.index');

        //media
        Route::get('/media', 'MediaController@index')->name('admin.media.index');
        Route::get('/media/popup/{popup_type?}/{type?}/{holder_attr?}/{related_id?}', 'MediaController@popup')->name('admin.media.popup');
        Route::post('/media/save', 'MediaController@save')->name('admin.media.save');
        Route::get('/media/edit/{id}', 'MediaController@edit')->name('admin.media.edit');
        Route::post('/media/store-extra/{id}', 'MediaController@storeExtra')->name('admin.media.store-extra');

        //offers
        Route::get('/offers/edit/{id}', 'OfferController@edit')->name('admin.offers.edit');
        Route::get('/offers/destroy/{id}', 'OfferController@destroy')->name('admin.offers.destroy');
        Route::get('/offers/create', 'OfferController@create')->name('admin.offers.create');
        Route::post('/offers/update', 'OfferController@update')->name('admin.offers.update');
        Route::get('/offers/change-status/{id}', 'OfferController@changeStatus')->name('admin.offers.change-status');
        Route::post('/offers/store', 'OfferController@store')->name('admin.offers.store');
        Route::get('/offers', 'OfferController@index')->name('admin.offers.index');

    	Route::get('vendor', 'VendorController@home');
        Route::get('vendor/home', 'VendorController@home')->name('admin.vendor.home');
        Route::get('vendor/create', 'VendorController@create')->name('admin.vendor.create');
        Route::get('vendor/edit/{id}', 'VendorController@edit')->name('admin.vendor.edit');
        Route::get('vendor/destroy/{id}', 'VendorController@destroy')->name('admin.vendor.destroy');
        Route::post('vendor/save', 'VendorController@save')->name('admin.vendor.save');
        Route::post('vendor/update', 'VendorController@update')->name('admin.vendor.update');

        Route::get('brand', 'BrandController@home')->name('admin.brand.home');
        Route::get('brand/create', 'BrandController@create')->name('admin.brand.create');
        Route::get('brand/edit/{id}', 'BrandController@edit')->name('admin.brand.edit');
        Route::get('brand/destroy/{id}', 'BrandController@destroy')->name('admin.brand.destroy');
        Route::post('brand/save', 'BrandController@save')->name('admin.brand.save');
        Route::post('brand/update', 'BrandController@update')->name('admin.brand.update');

        Route::get('category', 'CategoryController@home')->name('admin.category.home');
        Route::get('category/create', 'CategoryController@create')->name('admin.category.create');
        Route::get('category/edit/{id}', 'CategoryController@edit')->name('admin.category.edit');
        Route::get('category/destroy/{id}', 'CategoryController@destroy')->name('admin.category.destroy');
        Route::get('category/select2', 'CategoryController@category_select2')->name('admin.category.select2');

        Route::post('category/save', 'CategoryController@save')->name('admin.category.save');
        Route::post('category/update', 'CategoryController@update')->name('admin.category.update');

        //Category attributes
        Route::group(['prefix' => 'category', 'namespace' => 'Category'], function(){
            Route::get('/attributes/edit/{id}', 'AttributesController@edit')->name('admin.category.attribute.edit');
            Route::get('/attributes/create/{category_id?}', 'AttributesController@create')->name('admin.category.attribute.create');
            Route::post('/attributes/update', 'AttributesController@update')->name('admin.category.attribute.update');
            Route::post('/attributes/store', 'AttributesController@store')->name('admin.category.attribute.store');
            Route::get('/attributes/{category_id?}', 'AttributesController@index')->name('admin.category.attribute.index');
            Route::get('/attributes/destroy/{id}', 'AttributesController@destroy')->name('admin.category.attribute.destroy');

            Route::group(['prefix' => 'attribute', 'namespace' => 'Attribute'], function(){
            	Route::get('/groups/edit/{id}', 'GroupsController@edit')->name('admin.category.attribute.groups.edit');
                Route::get('/groups/create', 'GroupsController@create')->name('admin.category.attribute.groups.create');
                Route::post('/groups/update', 'GroupsController@update')->name('admin.category.attribute.groups.update');
                Route::post('/groups/store', 'GroupsController@store')->name('admin.category.attribute.groups.store');
                Route::get('/groups', 'GroupsController@index')->name('admin.category.attribute.groups.index');
                Route::get('/groups/destroy/{id}', 'GroupsController@destroy')->name('admin.category.attribute.groups.destroy');

                Route::get('/values/edit/{id}', 'ValuesController@edit')->name('admin.category.attribute.value.edit');
                Route::get('/values/create/{attribute}', 'ValuesController@create')->name('admin.category.attribute.value.create');
                Route::post('/values/update', 'ValuesController@update')->name('admin.category.attribute.value.update');
                Route::post('/values/store', 'ValuesController@store')->name('admin.category.attribute.value.store');
                Route::get('/{attribute}', 'ValuesController@index')->name('admin.category.attribute.value.index');
                Route::get('/values/destroy/{id}', 'ValuesController@destroy')->name('admin.category.attribute.value.destroy');
            });
        });


        Route::get('branch', 'BranchController@home')->name('admin.branch.home');
        Route::get('branch/create', 'BranchController@create')->name('admin.branch.create');
        Route::get('branch/edit/{id}', 'BranchController@edit')->name('admin.branch.edit');
        Route::get('branch/destroy/{id}', 'BranchController@destroy')->name('admin.branch.destroy');
        Route::get('branch/select2', 'BranchController@category_select2')->name('admin.branch.select2');

        Route::post('branch/save', 'BranchController@save')->name('admin.branch.save');
        Route::post('branch/update', 'BranchController@update')->name('admin.branch.update');

        Route::post('summernote/image', 'PluginController@summernote_image_upload')->name('admin.summernote.image');
    });
});
Route::group(['middleware' => ['isGuest']], function() {
    Route::get('/', 'Vanila\ShopController@home');
    Route::get('/cart', 'Vanila\ShopController@cart');
    Route::post('/product/variant', 'Vanila\ShopController@variant');
    Route::get('/product/{slug}', 'Vanila\ShopController@product');

    Route::post('/cart', 'Vanila\ShopController@addtocart');
    Route::post('/cart/count', 'Vanila\ShopController@cartcount');
    Route::post('/cart/total', 'Vanila\ShopController@carttotal');
    Route::post('/cart/remove', 'Vanila\ShopController@remove');

    Route::get('/popup/login', 'Vanila\ShopController@popup_login');
    Route::get('/popup/register', 'Vanila\ShopController@popup_register');

    Route::post('/signin', 'Vanila\ShopController@signin');
    Route::post('/signup', 'Vanila\ShopController@signup');

    Route::post('/signout', 'Vanila\ShopController@signout');
});

